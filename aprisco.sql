-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-09-2022 a las 18:04:28
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aprisco`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacoras`
--

CREATE TABLE `bitacoras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_usuario` bigint(20) UNSIGNED NOT NULL,
  `accion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `bitacoras`
--

INSERT INTO `bitacoras` (`id`, `id_usuario`, `accion`, `created_at`, `updated_at`) VALUES
(31, 3, 'Ingreso a módulo usuario', '2022-07-04 06:37:38', '2022-07-04 06:37:38'),
(32, 3, 'Ingreso a módulo ventas', '2022-07-04 06:37:39', '2022-07-04 06:37:39'),
(33, 3, 'Ingreso a módulo compras', '2022-07-04 06:37:40', '2022-07-04 06:37:40'),
(34, 3, 'Ingreso a módulo cliente', '2022-07-04 06:37:40', '2022-07-04 06:37:40'),
(35, 3, 'Ingreso a módulo proveedor', '2022-07-04 06:37:41', '2022-07-04 06:37:41'),
(36, 3, 'Ingreso a módulo producto', '2022-07-04 06:37:41', '2022-07-04 06:37:41'),
(37, 3, 'Ingreso a módulo compras', '2022-07-04 06:37:45', '2022-07-04 06:37:45'),
(38, 3, 'Ingreso a módulo usuario', '2022-07-04 06:38:52', '2022-07-04 06:38:52'),
(39, 3, 'Ingreso a módulo producto', '2022-07-04 06:38:59', '2022-07-04 06:38:59'),
(40, 3, 'Ingreso a módulo categoria', '2022-07-04 06:39:03', '2022-07-04 06:39:03'),
(41, 3, 'Creación de nueva categoria', '2022-07-04 06:39:15', '2022-07-04 06:39:15'),
(42, 3, 'Creación de nueva categoria', '2022-07-04 06:39:22', '2022-07-04 06:39:22'),
(43, 3, 'Creación de nueva categoria', '2022-07-04 06:39:29', '2022-07-04 06:39:29'),
(44, 3, 'Creación de nueva categoria', '2022-07-04 06:39:36', '2022-07-04 06:39:36'),
(45, 3, 'Creación de nueva categoria', '2022-07-04 06:39:42', '2022-07-04 06:39:42'),
(46, 3, 'Creación de nueva categoria', '2022-07-04 06:39:48', '2022-07-04 06:39:48'),
(47, 3, 'Eliminación de categoria', '2022-07-04 06:39:54', '2022-07-04 06:39:54'),
(48, 3, 'Eliminación de categoria', '2022-07-04 06:39:58', '2022-07-04 06:39:58'),
(49, 3, 'Eliminación de categoria', '2022-07-04 06:40:01', '2022-07-04 06:40:01'),
(50, 3, 'Creación de nueva categoria', '2022-07-04 06:40:08', '2022-07-04 06:40:08'),
(51, 3, 'Ingreso a módulo producto', '2022-07-04 06:40:38', '2022-07-04 06:40:38'),
(52, 3, 'Registro de nuevo producto', '2022-07-04 06:41:07', '2022-07-04 06:41:07'),
(53, 3, 'Registro de nuevo producto', '2022-07-04 06:41:32', '2022-07-04 06:41:32'),
(54, 3, 'Registro de nuevo producto', '2022-07-04 06:41:57', '2022-07-04 06:41:57'),
(55, 3, 'Registro de nuevo producto', '2022-07-04 06:42:18', '2022-07-04 06:42:18'),
(56, 3, 'Registro de nuevo producto', '2022-07-04 06:42:48', '2022-07-04 06:42:48'),
(57, 3, 'Registro de nuevo producto', '2022-07-04 06:43:14', '2022-07-04 06:43:14'),
(58, 3, 'Registro de nuevo producto', '2022-07-04 06:43:53', '2022-07-04 06:43:53'),
(59, 3, 'Registro de nuevo producto', '2022-07-04 06:45:20', '2022-07-04 06:45:20'),
(60, 3, 'Registro de nuevo producto', '2022-07-04 06:46:11', '2022-07-04 06:46:11'),
(61, 3, 'Registro de nuevo producto', '2022-07-04 06:47:07', '2022-07-04 06:47:07'),
(62, 3, 'Registro de nuevo producto', '2022-07-04 06:48:20', '2022-07-04 06:48:20'),
(63, 3, 'Registro de nuevo producto', '2022-07-04 06:48:59', '2022-07-04 06:48:59'),
(64, 3, 'Registro de nuevo producto', '2022-07-04 06:49:32', '2022-07-04 06:49:32'),
(65, 3, 'Registro de nuevo producto', '2022-07-04 06:50:11', '2022-07-04 06:50:11'),
(66, 3, 'Ingreso a módulo proveedor', '2022-07-04 06:50:17', '2022-07-04 06:50:17'),
(67, 3, 'Registro de nuevo proveedor', '2022-07-04 06:52:01', '2022-07-04 06:52:01'),
(68, 3, 'Registro de nuevo proveedor', '2022-07-04 06:52:52', '2022-07-04 06:52:52'),
(69, 3, 'Registro de nuevo proveedor', '2022-07-04 06:55:32', '2022-07-04 06:55:32'),
(70, 3, 'Registro de nuevo proveedor', '2022-07-04 06:56:33', '2022-07-04 06:56:33'),
(71, 3, 'Ingreso a módulo cliente', '2022-07-04 06:57:17', '2022-07-04 06:57:17'),
(72, 3, 'Registro de nuevo cliente', '2022-07-04 06:58:45', '2022-07-04 06:58:45'),
(73, 3, 'Registro de nuevo cliente', '2022-07-04 06:59:41', '2022-07-04 06:59:41'),
(74, 3, 'Registro de nuevo cliente', '2022-07-04 07:00:32', '2022-07-04 07:00:32'),
(75, 3, 'Ingreso a módulo cliente', '2022-07-04 07:00:45', '2022-07-04 07:00:45'),
(76, 3, 'Ingreso a módulo cliente', '2022-07-04 07:00:50', '2022-07-04 07:00:50'),
(77, 3, 'Registro de nuevo cliente', '2022-07-04 07:01:52', '2022-07-04 07:01:52'),
(78, 3, 'Registro de nuevo cliente', '2022-07-04 07:02:32', '2022-07-04 07:02:32'),
(79, 3, 'Registro de nuevo cliente', '2022-07-04 07:03:08', '2022-07-04 07:03:08'),
(80, 3, 'Registro de nuevo cliente', '2022-07-04 07:03:51', '2022-07-04 07:03:51'),
(81, 3, 'Registro de nuevo cliente', '2022-07-04 07:04:25', '2022-07-04 07:04:25'),
(82, 3, 'Registro de nuevo cliente', '2022-07-04 07:04:54', '2022-07-04 07:04:54'),
(83, 3, 'Registro de nuevo cliente', '2022-07-04 07:05:20', '2022-07-04 07:05:20'),
(84, 3, 'Registro de nuevo cliente', '2022-07-04 07:05:49', '2022-07-04 07:05:49'),
(85, 3, 'Registro de nuevo cliente', '2022-07-04 07:06:25', '2022-07-04 07:06:25'),
(86, 3, 'Ingreso a módulo trabajador', '2022-07-04 07:06:35', '2022-07-04 07:06:35'),
(87, 3, 'Ingreso a módulo usuario', '2022-07-04 07:06:42', '2022-07-04 07:06:42'),
(88, 3, 'Ingreso a módulo trabajador', '2022-07-04 07:06:44', '2022-07-04 07:06:44'),
(89, 3, 'Ingreso a módulo cliente', '2022-07-04 07:06:45', '2022-07-04 07:06:45'),
(90, 3, 'Ingreso a módulo proveedor', '2022-07-04 07:06:46', '2022-07-04 07:06:46'),
(91, 3, 'Ingreso a módulo trabajador', '2022-07-04 07:11:43', '2022-07-04 07:11:43'),
(92, 3, 'Ingreso a módulo cliente', '2022-07-04 07:11:44', '2022-07-04 07:11:44'),
(93, 3, 'Ingreso a módulo proveedor', '2022-07-04 07:11:46', '2022-07-04 07:11:46'),
(94, 3, 'Ingreso a módulo producto', '2022-07-04 07:11:47', '2022-07-04 07:11:47'),
(95, 3, 'Ingreso a módulo proveedor', '2022-07-04 07:11:48', '2022-07-04 07:11:48'),
(96, 3, 'Ingreso a módulo cliente', '2022-07-04 07:11:49', '2022-07-04 07:11:49'),
(97, 3, 'Ingreso a módulo trabajador', '2022-07-04 07:11:50', '2022-07-04 07:11:50'),
(98, 3, 'Registro de nuevo trabajador', '2022-07-04 07:12:46', '2022-07-04 07:12:46'),
(99, 3, 'Registro de nuevo trabajador', '2022-07-04 07:13:22', '2022-07-04 07:13:22'),
(100, 3, 'Registro de nuevo trabajador', '2022-07-04 07:14:43', '2022-07-04 07:14:43'),
(101, 3, 'Registro de nuevo trabajador', '2022-07-04 07:15:32', '2022-07-04 07:15:32'),
(102, 3, 'Registro de nuevo trabajador', '2022-07-04 07:16:35', '2022-07-04 07:16:35'),
(103, 3, 'Registro de nuevo trabajador', '2022-07-04 07:17:14', '2022-07-04 07:17:14'),
(104, 3, 'Ingreso a módulo ventas', '2022-07-04 07:17:55', '2022-07-04 07:17:55'),
(105, 3, 'Ingreso a módulo compras', '2022-07-04 07:17:56', '2022-07-04 07:17:56'),
(106, 3, 'Ingreso a módulo ventas', '2022-07-04 07:17:57', '2022-07-04 07:17:57'),
(107, 3, 'Ingreso a módulo ventas', '2022-07-04 07:17:58', '2022-07-04 07:17:58'),
(108, 3, 'Ingreso a módulo usuario', '2022-07-04 07:18:06', '2022-07-04 07:18:06'),
(109, 3, 'Ingreso a módulo ventas', '2022-07-04 07:18:16', '2022-07-04 07:18:16'),
(110, 3, 'Ingreso a módulo cliente', '2022-07-04 07:18:22', '2022-07-04 07:18:22'),
(111, 3, 'Ingreso a módulo trabajador', '2022-07-04 07:18:23', '2022-07-04 07:18:23'),
(112, 3, 'Ingreso a módulo trabajador', '2022-07-04 07:19:00', '2022-07-04 07:19:00'),
(113, 3, 'Ingreso a módulo trabajador', '2022-07-04 07:19:12', '2022-07-04 07:19:12'),
(114, 3, 'Ingreso a módulo trabajador', '2022-07-04 07:19:19', '2022-07-04 07:19:19'),
(115, 3, 'Ingreso a módulo trabajador', '2022-07-04 07:19:26', '2022-07-04 07:19:26'),
(116, 3, 'Ingreso a módulo compras', '2022-07-04 07:20:35', '2022-07-04 07:20:35'),
(117, 3, 'Ingreso a módulo ventas', '2022-07-04 07:20:36', '2022-07-04 07:20:36'),
(118, 3, 'Ingreso a módulo usuario', '2022-07-04 07:20:39', '2022-07-04 07:20:39'),
(119, 3, 'Ingreso a módulo producto', '2022-07-04 22:15:22', '2022-07-04 22:15:22'),
(120, 3, 'Registro de nuevo producto', '2022-07-04 22:16:56', '2022-07-04 22:16:56'),
(121, 3, 'Eliminación de producto', '2022-07-04 22:17:37', '2022-07-04 22:17:37'),
(122, 3, 'Actualización se producto', '2022-07-04 22:18:30', '2022-07-04 22:18:30'),
(123, 3, 'Ingreso a módulo categoria', '2022-07-04 22:18:40', '2022-07-04 22:18:40'),
(124, 3, 'Creación de nueva categoria', '2022-07-04 22:18:59', '2022-07-04 22:18:59'),
(125, 3, 'Ingreso a módulo proveedor', '2022-07-04 22:19:28', '2022-07-04 22:19:28'),
(126, 3, 'Ingreso a módulo compras', '2022-07-04 22:20:21', '2022-07-04 22:20:21'),
(127, 3, 'Ingreso a módulo nueva compra', '2022-07-04 22:20:33', '2022-07-04 22:20:33'),
(128, 3, 'Registro de compra', '2022-07-04 22:22:09', '2022-07-04 22:22:09'),
(129, 3, 'Ingreso a módulo compras', '2022-07-04 22:22:09', '2022-07-04 22:22:09'),
(130, 3, 'Ingreso a módulo producto', '2022-07-04 22:22:52', '2022-07-04 22:22:52'),
(131, 3, 'Ingreso a módulo ventas', '2022-07-04 22:23:03', '2022-07-04 22:23:03'),
(132, 3, 'Ingreso a módulo nueva venta', '2022-07-04 22:23:08', '2022-07-04 22:23:08'),
(133, 3, 'Ingreso a módulo nueva venta', '2022-07-04 22:24:29', '2022-07-04 22:24:29'),
(134, 3, 'Ingreso a módulo usuario', '2022-07-04 22:25:51', '2022-07-04 22:25:51'),
(135, 3, 'Registro de nuevo usuario', '2022-07-04 22:26:53', '2022-07-04 22:26:53'),
(136, 3, 'Ingreso a módulo de compras por fecha', '2022-07-04 22:27:02', '2022-07-04 22:27:02'),
(137, 3, 'Ingreso a módulo de compras por día', '2022-07-04 22:28:04', '2022-07-04 22:28:04'),
(138, 3, 'Ingreso a módulo de compras por fecha', '2022-07-04 22:28:22', '2022-07-04 22:28:22'),
(139, 3, 'Ingreso a módulo de ventas por fecha', '2022-07-04 22:28:53', '2022-07-04 22:28:53'),
(140, 3, 'Ingreso a módulo de ventas por fecha', '2022-07-04 22:32:02', '2022-07-04 22:32:02'),
(141, 3, 'Ingreso a módulo ventas', '2022-07-04 22:32:09', '2022-07-04 22:32:09'),
(142, 3, 'Ingreso a módulo nueva venta', '2022-07-04 22:32:12', '2022-07-04 22:32:12'),
(143, 3, 'Registro de venta', '2022-07-04 22:32:29', '2022-07-04 22:32:29'),
(144, 3, 'Ingreso a módulo ventas', '2022-07-04 22:32:29', '2022-07-04 22:32:29'),
(145, 3, 'Ingreso a módulo nueva venta', '2022-07-04 22:32:50', '2022-07-04 22:32:50'),
(146, 3, 'Registro de venta', '2022-07-04 22:33:54', '2022-07-04 22:33:54'),
(147, 3, 'Ingreso a módulo ventas', '2022-07-04 22:33:55', '2022-07-04 22:33:55'),
(148, 3, 'Ingreso a módulo producto', '2022-07-04 22:38:55', '2022-07-04 22:38:55'),
(149, 5, 'Ingreso a módulo trabajador', '2022-07-04 22:39:58', '2022-07-04 22:39:58'),
(150, 3, 'Ingreso a módulo de compras por fecha', '2022-07-04 22:42:55', '2022-07-04 22:42:55'),
(151, 3, 'Ingreso a módulo de compras por fecha', '2022-07-04 22:43:14', '2022-07-04 22:43:14'),
(152, 3, 'Ingreso a módulo trabajador', '2022-07-04 23:24:45', '2022-07-04 23:24:45'),
(153, 3, 'Ingreso a módulo proveedor', '2022-07-04 23:24:47', '2022-07-04 23:24:47'),
(154, 3, 'Ingreso a módulo producto', '2022-07-04 23:24:48', '2022-07-04 23:24:48'),
(155, 3, 'Ingreso a módulo producto', '2022-07-16 03:37:34', '2022-07-16 03:37:34'),
(156, 3, 'Ingreso a módulo ventas', '2022-07-16 03:37:41', '2022-07-16 03:37:41'),
(157, 3, 'Ingreso a módulo compras', '2022-07-16 03:37:43', '2022-07-16 03:37:43'),
(158, 3, 'Ingreso a módulo trabajador', '2022-07-16 03:37:46', '2022-07-16 03:37:46'),
(159, 3, 'Ingreso a módulo proveedor', '2022-07-16 03:37:47', '2022-07-16 03:37:47'),
(160, 3, 'Ingreso a módulo compras', '2022-07-16 03:37:47', '2022-07-16 03:37:47'),
(161, 3, 'Ingreso a módulo ventas', '2022-07-16 03:37:49', '2022-07-16 03:37:49'),
(162, 3, 'Ingreso a módulo trabajador', '2022-07-16 03:37:52', '2022-07-16 03:37:52'),
(163, 3, 'Ingreso a módulo producto', '2022-09-05 19:46:29', '2022-09-05 19:46:29'),
(164, 3, 'Ingreso a módulo cliente', '2022-09-05 19:46:29', '2022-09-05 19:46:29'),
(165, 3, 'Ingreso a módulo compras', '2022-09-05 19:46:32', '2022-09-05 19:46:32'),
(166, 3, 'Ingreso a módulo ventas', '2022-09-05 19:46:32', '2022-09-05 19:46:32'),
(167, 3, 'Ingreso a módulo usuario', '2022-09-05 19:46:35', '2022-09-05 19:46:35'),
(168, 3, 'Ingreso a módulo de compras por fecha', '2022-09-05 19:46:39', '2022-09-05 19:46:39'),
(169, 3, 'Ingreso a módulo ventas', '2022-09-05 19:46:47', '2022-09-05 19:46:47'),
(170, 3, 'Ingreso a módulo compras', '2022-09-05 19:46:48', '2022-09-05 19:46:48'),
(171, 3, 'Ingreso a módulo cliente', '2022-09-05 19:46:48', '2022-09-05 19:46:48'),
(172, 3, 'Ingreso a módulo proveedor', '2022-09-05 19:46:49', '2022-09-05 19:46:49'),
(173, 3, 'Ingreso a módulo producto', '2022-09-05 19:46:49', '2022-09-05 19:46:49'),
(174, 3, 'Ingreso a módulo usuario', '2022-09-05 19:58:41', '2022-09-05 19:58:41'),
(175, 5, 'Ingreso a módulo ventas', '2022-09-05 19:58:58', '2022-09-05 19:58:58'),
(176, 5, 'Ingreso a módulo compras', '2022-09-05 19:58:58', '2022-09-05 19:58:58'),
(177, 5, 'Ingreso a módulo trabajador', '2022-09-05 19:59:02', '2022-09-05 19:59:02'),
(178, 5, 'Ingreso a módulo cliente', '2022-09-05 19:59:02', '2022-09-05 19:59:02'),
(179, 5, 'Ingreso a módulo proveedor', '2022-09-05 19:59:03', '2022-09-05 19:59:03'),
(180, 5, 'Ingreso a módulo producto', '2022-09-05 19:59:04', '2022-09-05 19:59:04'),
(181, 5, 'Ingreso a módulo producto', '2022-09-05 19:59:04', '2022-09-05 19:59:04'),
(182, 5, 'Ingreso a módulo proveedor', '2022-09-05 19:59:05', '2022-09-05 19:59:05'),
(183, 5, 'Ingreso a módulo cliente', '2022-09-05 19:59:05', '2022-09-05 19:59:05'),
(184, 5, 'Ingreso a módulo trabajador', '2022-09-05 19:59:06', '2022-09-05 19:59:06'),
(185, 3, 'Ingreso a módulo usuario', '2022-09-05 20:00:48', '2022-09-05 20:00:48'),
(186, 3, 'Ingreso a módulo cliente', '2022-09-05 20:02:35', '2022-09-05 20:02:35'),
(187, 3, 'Ingreso a módulo cliente', '2022-09-05 20:02:36', '2022-09-05 20:02:36'),
(188, 3, 'Ingreso a módulo ventas', '2022-09-05 20:02:37', '2022-09-05 20:02:37'),
(189, 5, 'Ingreso a módulo trabajador', '2022-09-05 20:02:48', '2022-09-05 20:02:48'),
(190, 5, 'Ingreso a módulo compras', '2022-09-05 20:02:49', '2022-09-05 20:02:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `categoria_nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `categoria_nombre`, `created_at`, `updated_at`) VALUES
(1, 'Galletas', '2022-06-29 02:37:12', '2022-06-29 02:37:12'),
(2, 'Viveres', '2022-07-04 06:39:15', '2022-07-04 06:39:15'),
(3, 'Quesos', '2022-07-04 06:39:22', '2022-07-04 06:39:22'),
(4, 'Lacteos', '2022-07-04 06:39:29', '2022-07-04 06:39:29'),
(8, 'Jamones', '2022-07-04 06:40:08', '2022-07-04 06:40:08'),
(9, 'Bebidas', '2022-07-04 22:18:59', '2022-07-04 22:18:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre_cliente` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido_cliente` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre_cliente`, `apellido_cliente`, `tipo_documento`, `dni`, `telefono`, `email`, `direccion`, `created_at`, `updated_at`) VALUES
(1, 'Cristian', 'Sanchez', 'V', '71951141', '04248714141', 'cristian@cristian.com', '5 av.', '2022-07-04 06:58:45', '2022-07-04 06:58:45'),
(2, 'Martina', 'Orellana', 'V', '4279660952', '4279660952', 'aleix73@terra.com', 'Cl. Santiago, Casa 14', '2022-07-04 06:59:41', '2022-07-04 06:59:41'),
(3, 'Mara', 'Gallego', 'V', '042487713041', '2749638183', 'delatorre.juan@gmail.com', 'Av. Ruben, Hab. 8', '2022-07-04 07:00:32', '2022-07-04 07:00:32'),
(4, 'Adrian', 'Lerma', 'V', '1127150710', '581651210', 'espinal.hector@velazquez.com', 'Av. Lucas, Hab. 4', '2022-07-04 07:01:52', '2022-07-04 07:01:52'),
(5, 'Maria', 'Tua', 'V', '7517141', '4098655809', 'dpardo@adorno.net.ve', 'Callejón Alejandra Vázquez, Nro 4', '2022-07-04 07:02:32', '2022-07-04 07:02:32'),
(6, 'Maria', 'Tua', 'V', '141710417', '584144340722', 'jorge67@hotmail.com', 'Calle Hector Enríquez, 399, Piso 94', '2022-07-04 07:03:08', '2022-07-04 07:03:08'),
(7, 'Vega', 'Burgos', 'V', '441151014', '5821495', 'ines.mata@murillo.net.ve', 'Vereda Valentina Quintero, Piso 72', '2022-07-04 07:03:51', '2022-07-04 07:03:51'),
(8, 'Anna', 'Arce', 'V', '7505011', '2331216981', 'mbanuelos@yahoo.es', 'Carretera Isaac Saucedo, Apto 1', '2022-07-04 07:04:25', '2022-07-04 07:04:25'),
(9, 'Juan', 'Villalobos', 'V', '35417121', '4692485184', 'fechevarria@gmail.com', 'Callejón Francisco Javier Villareal, 03, Hab. 02', '2022-07-04 07:04:54', '2022-07-04 07:04:54'),
(10, 'Jesus', 'Alvarez', 'V', '3541721', '04145143318', 'jesuspascual@gmail.com', 'Las doñas', '2022-07-04 07:05:20', '2022-07-04 07:05:20'),
(11, 'Tadiana', 'Alvarez', 'V', '10517631', '04145143345', 'tadiana@gmail.com', 'Las doñas', '2022-07-04 07:05:49', '2022-07-04 07:05:49'),
(12, 'Carolina', 'Aldazora', 'V', '16471117', '117171', 'carolina@gmail.com', 'Urbanización los chaguaramos', '2022-07-04 07:06:25', '2022-07-04 07:06:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fecha_compra` date NOT NULL,
  `total_pagar` decimal(20,2) NOT NULL,
  `id_proveedor` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`id`, `fecha_compra`, `total_pagar`, `id_proveedor`, `created_at`, `updated_at`) VALUES
(1, '2022-06-28', '50.00', 1, '2022-06-29 02:42:13', '2022-06-29 02:42:13'),
(2, '2022-07-04', '500.00', 2, '2022-07-04 22:22:09', '2022-07-04 22:22:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_compras`
--

CREATE TABLE `detalle_compras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cantidad` int(11) NOT NULL,
  `costo` decimal(8,2) NOT NULL,
  `precio` decimal(8,2) NOT NULL,
  `compra_id` bigint(20) UNSIGNED NOT NULL,
  `id_producto` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_compras`
--

INSERT INTO `detalle_compras` (`id`, `cantidad`, `costo`, `precio`, `compra_id`, `id_producto`, `created_at`, `updated_at`) VALUES
(1, 50, '1.00', '1.50', 1, 1, '2022-06-29 02:42:13', '2022-06-29 02:42:13'),
(2, 500, '1.00', '2.00', 2, 3, '2022-07-04 22:22:09', '2022-07-04 22:22:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_ventas`
--

CREATE TABLE `detalle_ventas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(8,2) NOT NULL,
  `venta_id` bigint(20) UNSIGNED NOT NULL,
  `id_producto` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_ventas`
--

INSERT INTO `detalle_ventas` (`id`, `cantidad`, `precio`, `venta_id`, `id_producto`, `created_at`, `updated_at`) VALUES
(1, 10, '2.00', 3, 12, '2022-07-04 22:32:29', '2022-07-04 22:32:29'),
(2, 10, '0.90', 4, 8, '2022-07-04 22:33:54', '2022-07-04 22:33:54'),
(3, 5, '1.00', 4, 6, '2022-07-04 22:33:54', '2022-07-04 22:33:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medida`
--

CREATE TABLE `medida` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre_medida` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `medida`
--

INSERT INTO `medida` (`id`, `nombre_medida`, `created_at`, `updated_at`) VALUES
(1, 'L.', NULL, NULL),
(2, 'K.', NULL, NULL),
(3, 'Unidades', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_05_13_001641_create_tipo_usuario_table', 1),
(2, '2013_05_16_203402_create_medida_table', 1),
(3, '2013_05_18_153541_create_categorias_table', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(6, '2022_04_01_192445_create_sessions_table', 1),
(7, '2022_04_01_192639_create_proveedors_table', 1),
(8, '2022_05_20_005745_create_productos_table', 1),
(9, '2022_05_20_005746_create_clientes_table', 1),
(10, '2022_05_26_205641_create_trabajador_table', 1),
(11, '2022_05_30_144545_create_bitacoras_table', 1),
(12, '2022_05_30_205003_create_compras_table', 1),
(13, '2022_05_30_205004_create_detalle_compras_table', 1),
(14, '2022_05_30_205014_create_ventas_table', 1),
(15, '2022_05_30_205023_create_detalle_venta_table', 1),
(16, '2022_06_28_200427_create_permission_tables', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 3),
(1, 'App\\Models\\User', 4),
(1, 'App\\Models\\User', 7),
(2, 'App\\Models\\User', 5),
(2, 'App\\Models\\User', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'main', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(2, 'bitacoras', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(3, 'reportes', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(4, 'reportes.compraf', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(5, 'reportes.comprad', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(6, 'reportes.ventaf', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(7, 'reportes.ventad', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(8, 'compras', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(9, 'compras.show', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(10, 'compras.create', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(11, 'compras.destroy', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(12, 'ventas', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(13, 'ventas.show', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(14, 'ventas.create', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(15, 'ventas.destroy', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(16, 'categoriasData', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(17, 'categoriasCreate', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(18, 'categoriasupdate', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(19, 'categoriasDestroy', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(20, 'categoriasfind', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(21, 'productoData', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(22, 'productosCreate', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(23, 'productosupdate', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(24, 'productosDestroy', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(25, 'productosfind', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(26, 'proveedorData', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(27, 'proveedorCreate', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(28, 'proveedorupdate', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(29, 'proveedorDestroy', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(30, 'proveedorfind', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(31, 'clienteData', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(32, 'clienteCreate', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(33, 'clienteupdate', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(34, 'clienteDestroy', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(35, 'clientefind', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(36, 'usuariosData', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(37, 'usuariosCreate', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(38, 'usuariosupdate', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(39, 'usuariosDestroy', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(40, 'usuariosfind', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(41, 'trabajadorData', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(42, 'trabajadorCreate', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(43, 'trabajadorupdate', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(44, 'trabajadorDestroy', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00'),
(45, 'trabajadorfind', 'web', '2022-06-29 02:25:00', '2022-06-29 02:25:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `codigo` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(8,2) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_tipo_producto` bigint(20) UNSIGNED NOT NULL,
  `id_medida` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `codigo`, `nombre`, `cantidad`, `precio`, `descripcion`, `id_tipo_producto`, `id_medida`, `created_at`, `updated_at`) VALUES
(1, 'COD221', 'Galletas Oreo Vani', 90, '1.50', 'Galletas Oreo de Vainilla', 1, 3, '2022-06-29 02:37:48', '2022-06-29 02:37:48'),
(2, 'COD331', 'Queso llanero', 40, '5.00', 'Queso llanero Blanco', 3, 2, '2022-07-04 06:41:07', '2022-07-04 06:41:07'),
(3, 'COD33134', 'Queso amarillo', 80, '2.00', 'Queso Amarillo', 3, 2, '2022-07-04 06:41:32', '2022-07-04 06:41:32'),
(4, 'COD5534', 'Jamón Plumrose', 20, '9.00', 'Jamón de pierna', 8, 2, '2022-07-04 06:41:57', '2022-07-04 06:41:57'),
(5, 'COD5444', 'Jamón de pavo', 40, '7.50', 'Jamón de pavo', 8, 2, '2022-07-04 06:42:18', '2022-07-04 06:42:18'),
(6, 'COD5777', 'Leche Purisima', 55, '1.00', 'Leche purisima descremada', 4, 1, '2022-07-04 06:42:48', '2022-07-04 06:42:48'),
(7, 'COD5744', 'Leche la pastoreña', 30, '1.00', 'Leche la pastoreña completa', 4, 1, '2022-07-04 06:43:14', '2022-07-04 06:43:14'),
(8, 'COD57455', 'Harina Pan', 40, '0.90', 'Harina Pan amarilla', 2, 2, '2022-07-04 06:43:53', '2022-07-04 06:43:53'),
(10, 'COD54955', 'Harina Doña emilia', 100, '1.50', 'Harina de maiz', 2, 2, '2022-07-04 06:46:11', '2022-07-04 22:18:30'),
(11, 'COD1155', 'Aceite Vatel', 40, '2.50', 'Aceita de soya', 2, 1, '2022-07-04 06:47:07', '2022-07-04 06:47:07'),
(12, 'COD115567', 'Aceite coamo', 30, '2.00', 'Aceita de soya', 2, 1, '2022-07-04 06:48:20', '2022-07-04 06:48:20'),
(13, 'COD157', 'Galletas Chips Ahoy', 20, '3.50', 'Galletas de chocolate Chips Ahoy!', 1, 3, '2022-07-04 06:48:59', '2022-07-04 06:48:59'),
(14, 'COD15787', 'Galletas Festival Fresa', 20, '0.50', 'Galletas festival de fresa', 1, 3, '2022-07-04 06:49:32', '2022-07-04 06:49:32'),
(15, 'COD179131', 'Suero San Pedro', 5, '1.50', 'Suero San pedro', 4, 1, '2022-07-04 06:50:11', '2022-07-04 06:50:11'),
(16, 'COD2502', 'Queso mozarella', 10, '15.00', 'queso mozarella', 3, 2, '2022-07-04 22:16:56', '2022-07-04 22:16:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedors`
--

CREATE TABLE `proveedors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre_proveedor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_vendedor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion_fiscal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `proveedors`
--

INSERT INTO `proveedors` (`id`, `nombre_proveedor`, `tipo_documento`, `dni`, `email`, `telefono`, `nombre_vendedor`, `direccion_fiscal`, `created_at`, `updated_at`) VALUES
(1, 'Inversiones la Nena 2010', 'J', '75718514044', 'inversionesnena@gmail.com', '02517171286', 'Juan Vasquez', 'calle 51 entre 24 y 25', '2022-06-29 02:39:26', '2022-06-29 02:39:26'),
(2, 'Bodegon La criolla', 'J', '719116204', 'lacriolla@lacriolla.com', '02517571781', 'No aplica', 'Av libertador', '2022-07-04 06:52:01', '2022-07-04 06:52:01'),
(3, 'Jose Fernandez', 'V', '791112046', 'josefernandez@gmail.com', '0424815154', 'Jose Fernandez', 'Calle 53 con carrera 21', '2022-07-04 06:52:52', '2022-07-04 06:52:52'),
(4, 'Gladys Gutierrez', 'V', '11268817', 'gladys@gmail.com', '04148816220', 'No aplica', 'No aplica', '2022-07-04 06:55:32', '2022-07-04 06:55:32'),
(5, 'Alimentos Polar', 'J', '715110443', 'polar@polar.com', '02121778141', 'Mario Gil', 'Pedro León Torres', '2022-07-04 06:56:33', '2022-07-04 06:56:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'administrador', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59'),
(2, 'cajero', 'web', '2022-06-29 02:24:59', '2022-06-29 02:24:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(8, 2),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 1),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(16, 1),
(16, 2),
(17, 1),
(17, 2),
(18, 1),
(19, 1),
(20, 1),
(20, 2),
(21, 1),
(21, 2),
(22, 1),
(22, 2),
(23, 1),
(24, 1),
(25, 1),
(25, 2),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(29, 1),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(32, 1),
(32, 2),
(33, 1),
(34, 1),
(35, 1),
(35, 2),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(41, 2),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(45, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('9TnCxesvIUAZ4N5OUAzy6P0WbhInH4GEGRR4dIY1', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiNXc2NUxSMldQOHowYUtzMTRscG9HeFRMczlKQ2N5TDFldFViR0lYVyI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMCI7fX0=', 1662393771);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipo_usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id`, `tipo_usuario`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', NULL, NULL),
(2, 'Cajero', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadors`
--

CREATE TABLE `trabajadors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre_trabajador` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido_trabajador` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_trabajador` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `trabajadors`
--

INSERT INTO `trabajadors` (`id`, `nombre_trabajador`, `apellido_trabajador`, `tipo_documento`, `dni`, `cargo_trabajador`, `email`, `telefono`, `direccion`, `fecha_ingreso`, `created_at`, `updated_at`) VALUES
(1, 'Roberto', 'Del rio', 'V', '951711171', 'Cajero', 'madrigal.raul@moreno.net', '584121411', 'Av. Sandoval, Nro 29', '2022-02-03', '2022-07-04 07:12:46', '2022-07-04 07:12:46'),
(2, 'Raul', 'Sanchez', 'V', '871211392', 'Cajero', 'flongoria@terra.com', '04120126868', 'Vereda Andres Moran, Apto 38', '2022-02-26', '2022-07-04 07:13:22', '2022-07-04 07:13:22'),
(3, 'Saul', 'Serna', 'V', '181711520', 'Almacen', 'jan93@terra.com', '04120247717', 'Avenida Eric, Hab. 9', '2022-02-23', '2022-07-04 07:14:43', '2022-07-04 07:14:43'),
(4, 'Francisco', 'Sevilla', 'V', '25171711', 'Almacen', 'ainara60@delgado.com.ve', '04120444', 'Av. Domenech, 103, Apto 7', '2022-02-23', '2022-07-04 07:15:32', '2022-07-04 07:15:32'),
(5, 'Valeria', 'Camarro', 'V', '18171111', 'Supervisora', 'valles.leandro@gmail.com', '0412181711', 'Cl. Carolina Collado, Piso 0', '2021-11-19', '2022-07-04 07:16:35', '2022-07-04 07:16:35'),
(6, 'Joel', 'Ramirez', 'V', '3434234123', 'Gerente', 'samuel.malave@marcos.info.ve', '4234241233', 'Av. Valverde, Apto 46', '2021-07-08', '2022-07-04 07:17:14', '2022-07-04 07:17:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_tipo_usuario` bigint(20) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `id_tipo_usuario`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Administrador', 'admin@admin.com', NULL, '$2y$10$C00XZzA9fO6AG9JVbIUJEuBhkwaImq5ZE8nEdeIqUDlh8uTkii6kq', 1, 'rDFqqhknV11YQb3s5FJsvPQp7OJJ3pAmjIqVGzQkq52dUZY4lQh3Mk6LuFuP', '2022-06-29 02:27:12', '2022-06-29 02:27:12'),
(4, 'Administrador1', 'administrador@administrador', NULL, '$2y$10$2z10hJzG8ysK5m.WxMmVeuKc4Rfsw7/4NGb8ov.oJTbiKOfgAFmXe', 1, NULL, '2022-06-29 02:28:16', '2022-07-04 06:34:00'),
(5, 'Cajero', 'cajero@cajero.com', NULL, '$2y$10$qZgt1ZCTf5cUrxh2kzOHXemQMxuqhMBrxW3kiCsB.JjEE.ouhK.MW', 2, NULL, '2022-06-29 02:28:38', '2022-06-29 02:28:38'),
(6, 'Cajero1', 'cajero1@cajero.com', NULL, '$2y$10$nPBg0I20xIdmTFOXIWsFKuy7EEVImFnYUcKeVWhO.rt/f9q7LP5s.', 2, NULL, '2022-06-29 02:28:47', '2022-06-29 02:28:47'),
(7, 'Mafer prueba', 'maferprueba@gmail.com', NULL, '$2y$10$KARaIfdpEtkqKJClGTOsgOgKQzb9W4t7JEa25aHTNmxSQnH5y8ayy', 1, NULL, '2022-07-04 22:26:53', '2022-07-04 22:26:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fecha_venta` date NOT NULL,
  `total_pagar` decimal(8,2) NOT NULL,
  `id_cliente` bigint(20) UNSIGNED NOT NULL,
  `id_trabajador` bigint(20) UNSIGNED NOT NULL,
  `metodo_pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banco_pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `fecha_venta`, `total_pagar`, `id_cliente`, `id_trabajador`, `metodo_pago`, `banco_pago`, `created_at`, `updated_at`) VALUES
(1, '2022-07-04', '800.00', 1, 2, 'Bs. Efectivo', 'Banco Provincial', '2022-07-04 22:24:11', '2022-07-04 22:24:11'),
(2, '2022-07-04', '140.00', 1, 3, 'Bs. Efectivo', 'Banco Provincial', '2022-07-04 22:25:22', '2022-07-04 22:25:22'),
(3, '2022-07-04', '20.00', 4, 3, 'Bs. Efectivo', 'Banco Provincial', '2022-07-04 22:32:29', '2022-07-04 22:32:29'),
(4, '2022-07-04', '14.00', 1, 2, 'Bs. Efectivo', 'Banco Provincial', '2022-07-04 22:33:54', '2022-07-04 22:33:54');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bitacoras`
--
ALTER TABLE `bitacoras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bitacoras_id_usuario_foreign` (`id_usuario`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorias_categoria_nombre_unique` (`categoria_nombre`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clientes_dni_unique` (`dni`),
  ADD UNIQUE KEY `clientes_telefono_unique` (`telefono`),
  ADD UNIQUE KEY `clientes_email_unique` (`email`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `compras_id_proveedor_foreign` (`id_proveedor`);

--
-- Indices de la tabla `detalle_compras`
--
ALTER TABLE `detalle_compras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detalle_compras_compra_id_foreign` (`compra_id`),
  ADD KEY `detalle_compras_id_producto_foreign` (`id_producto`);

--
-- Indices de la tabla `detalle_ventas`
--
ALTER TABLE `detalle_ventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detalle_venta_venta_id_foreign` (`venta_id`),
  ADD KEY `detalle_venta_id_producto_foreign` (`id_producto`);

--
-- Indices de la tabla `medida`
--
ALTER TABLE `medida`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `medida_nombre_medida_unique` (`nombre_medida`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `productos_codigo_unique` (`codigo`),
  ADD UNIQUE KEY `productos_nombre_unique` (`nombre`),
  ADD KEY `productos_id_tipo_producto_foreign` (`id_tipo_producto`),
  ADD KEY `productos_id_medida_foreign` (`id_medida`);

--
-- Indices de la tabla `proveedors`
--
ALTER TABLE `proveedors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `proveedors_nombre_proveedor_unique` (`nombre_proveedor`),
  ADD UNIQUE KEY `proveedors_dni_unique` (`dni`),
  ADD UNIQUE KEY `proveedors_email_unique` (`email`),
  ADD UNIQUE KEY `proveedors_telefono_unique` (`telefono`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tipo_usuario_tipo_usuario_unique` (`tipo_usuario`);

--
-- Indices de la tabla `trabajadors`
--
ALTER TABLE `trabajadors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trabajadors_dni_unique` (`dni`),
  ADD UNIQUE KEY `trabajadors_email_unique` (`email`),
  ADD UNIQUE KEY `trabajadors_telefono_unique` (`telefono`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_email_verified_at_unique` (`email_verified_at`),
  ADD KEY `users_id_tipo_usuario_foreign` (`id_tipo_usuario`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ventas_id_cliente_foreign` (`id_cliente`),
  ADD KEY `ventas_id_trabajador_foreign` (`id_trabajador`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bitacoras`
--
ALTER TABLE `bitacoras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `detalle_compras`
--
ALTER TABLE `detalle_compras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `detalle_ventas`
--
ALTER TABLE `detalle_ventas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `medida`
--
ALTER TABLE `medida`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `proveedors`
--
ALTER TABLE `proveedors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `trabajadors`
--
ALTER TABLE `trabajadors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bitacoras`
--
ALTER TABLE `bitacoras`
  ADD CONSTRAINT `bitacoras_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `compras_id_proveedor_foreign` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedors` (`id`);

--
-- Filtros para la tabla `detalle_compras`
--
ALTER TABLE `detalle_compras`
  ADD CONSTRAINT `detalle_compras_compra_id_foreign` FOREIGN KEY (`compra_id`) REFERENCES `compras` (`id`),
  ADD CONSTRAINT `detalle_compras_id_producto_foreign` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`);

--
-- Filtros para la tabla `detalle_ventas`
--
ALTER TABLE `detalle_ventas`
  ADD CONSTRAINT `detalle_venta_id_producto_foreign` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`),
  ADD CONSTRAINT `detalle_venta_venta_id_foreign` FOREIGN KEY (`venta_id`) REFERENCES `ventas` (`id`);

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_id_medida_foreign` FOREIGN KEY (`id_medida`) REFERENCES `medida` (`id`),
  ADD CONSTRAINT `productos_id_tipo_producto_foreign` FOREIGN KEY (`id_tipo_producto`) REFERENCES `categorias` (`id`);

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_tipo_usuario_foreign` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `tipo_usuario` (`id`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_id_cliente_foreign` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`),
  ADD CONSTRAINT `ventas_id_trabajador_foreign` FOREIGN KEY (`id_trabajador`) REFERENCES `trabajadors` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
