<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::create(['name' => 'administrador']);
        $role2 = Role::create(['name' => 'cajero']);

        Permission::create(['name' => 'main'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'bitacoras'])->syncRoles([$role1]);

        Permission::create(['name' => 'reportes'])->syncRoles([$role1]);
        Permission::create(['name' => 'reportes.compraf'])->syncRoles([$role1]);
        Permission::create(['name' => 'reportes.comprad'])->syncRoles([$role1]);
        Permission::create(['name' => 'reportes.ventaf'])->syncRoles([$role1]);
        Permission::create(['name' => 'reportes.ventad'])->syncRoles([$role1]);

        Permission::create(['name' => 'compras'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'compras.show'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'compras.create'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'compras.destroy'])->syncRoles([$role1]);

        Permission::create(['name' => 'ventas'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'ventas.show'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'ventas.create'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'ventas.destroy'])->syncRoles([$role1]);

        Permission::create(['name' => 'categoriasData'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'categoriasCreate'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'categoriasupdate'])->syncRoles([$role1]);
        Permission::create(['name' => 'categoriasDestroy'])->syncRoles([$role1]);
        Permission::create(['name' => 'categoriasfind'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'productoData'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'productosCreate'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'productosupdate'])->syncRoles([$role1]);
        Permission::create(['name' => 'productosDestroy'])->syncRoles([$role1]);
        Permission::create(['name' => 'productosfind'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'proveedorData'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'proveedorCreate'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'proveedorupdate'])->syncRoles([$role1]);
        Permission::create(['name' => 'proveedorDestroy'])->syncRoles([$role1]);
        Permission::create(['name' => 'proveedorfind'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'clienteData'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'clienteCreate'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'clienteupdate'])->syncRoles([$role1]);
        Permission::create(['name' => 'clienteDestroy'])->syncRoles([$role1]);
        Permission::create(['name' => 'clientefind'])->syncRoles([$role1, $role2]);

        Permission::create(['name' => 'usuariosData'])->syncRoles([$role1]);
        Permission::create(['name' => 'usuariosCreate'])->syncRoles([$role1]);
        Permission::create(['name' => 'usuariosupdate'])->syncRoles([$role1]);
        Permission::create(['name' => 'usuariosDestroy'])->syncRoles([$role1]);
        Permission::create(['name' => 'usuariosfind'])->syncRoles([$role1]);

        Permission::create(['name' => 'trabajadorData'])->syncRoles([$role1, $role2]);
        Permission::create(['name' => 'trabajadorCreate'])->syncRoles([$role1]);
        Permission::create(['name' => 'trabajadorupdate'])->syncRoles([$role1]);
        Permission::create(['name' => 'trabajadorDestroy'])->syncRoles([$role1]);
        Permission::create(['name' => 'trabajadorfind'])->syncRoles([$role1, $role2]);
    }
}
