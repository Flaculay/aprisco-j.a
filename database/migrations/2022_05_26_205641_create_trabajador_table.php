<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabajadors', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_trabajador');
            $table->string('apellido_trabajador');
            $table->string('tipo_documento');
            $table->string('dni')->unique();
            $table->string('cargo_trabajador');
            $table->string('email')->unique();
            $table->string('telefono')->unique();
            $table->string('direccion');
            $table->date('fecha_ingreso');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trabajador');
    }
};
