<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Models\Bitacora;
use Illuminate\Support\Facades\Auth;

class ClienteController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo cliente';
        $bitacoras->save();
        return view('cliente.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'dni' => 'required|unique:clientes|max:255|regex:(^[0-9])',
            'telefono' => 'required|unique:clientes|max:255|regex:(^[0-9])',
            'email' => 'required|unique:clientes',
            'nombre_cliente' => 'required|max:255|regex:(^[a-zA-Z ])',
            'apellido_cliente' => 'required|max:255|regex:(^[a-zA-Z ])',
            'direccion' => 'required|max:255|regex:(^[a-zA-Z0-9 .-])',
        ]);


        $clientes = new Cliente();
        $clientes->create($request->all());

        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'cliente registrado con exito',
            'code' => 'success',
        ];

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Registro de nuevo cliente';
        $bitacoras->save();
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   

        $clientes = Cliente::all();
        foreach($clientes as $cliente){

            $cliente->botones =  sprintf(
                "  <button class='btn btn-warning btn-sm process' data-dni='%s' data-toggle='modal' data-target='#modalShow'><i class='fa-solid fa-eye'></i> Detalles</button>",
                $cliente->dni
            );

            $cliente->botones .=  sprintf(
                "  <button class='btn btn-light btn-sm process' data-dni='%s'  data-toggle='modal' data-target='#modalEdit'><i class='fa-solid fa-pen-to-square'></i> Editar</button>", 
                $cliente->dni
            );

            $cliente->botones .=  sprintf(
                "  <button class='btn btn-danger btn-sm delete' data-dni='%s'><i class='fa-solid fa-trash-can'></i> Eliminar</button>",  
                $cliente->dni
            );
        }

        return $clientes;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validated = $request->validate([
            'dni' => 'required|max:255|regex:(^[0-9])',
            'telefono' => 'required|max:255|regex:(^[0-9])',
            'email' => 'required',
            'nombre_cliente' => 'required|max:255|regex:(^[a-zA-Z ])',
            'apellido_cliente' => 'required|max:255|regex:(^[a-zA-Z ])',
            'direccion' => 'required|max:255|regex:(^[a-zA-Z0-9 .-])',
        ]);

        $id=$request->id;
        $clientes=Cliente::find($id);
        $clientes->update($request->all());
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Cliente actualizado con exito',
            'code' => 'success',
        ];

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Actualización de cliente';
        $bitacoras->save();
        
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $dni=$request->dni;
        $clientes=\DB::table('clientes') ->where('dni', '=', $dni);
        $clientes->delete();
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Cliente eliminado con exito',
            'code' => 'success',
        ];

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Eliminación de cliente';
        $bitacoras->save();

        return $data;
    }

    public function find(Request $request) {

        $dni=$request->dni;
        $clientes=\DB::table('clientes') ->where('dni', '=', $dni)->get();
        return $clientes[0];
    }
}
