<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trabajador;
use App\Models\Bitacora;
use Illuminate\Support\Facades\Auth;

class TrabajadorController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo trabajador';
        $bitacoras->save();
        return view('trabajador.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'dni' => 'required|unique:trabajadors|max:255|regex:(^[0-9])',
            'telefono' => 'required|unique:trabajadors|max:255|regex:(^[0-9])',
            'email' => 'required|unique:trabajadors|max:255',
            'nombre_trabajador' => 'required|max:255|regex:(^[a-zA-Z ])',
            'cargo_trabajador' => 'required|max:255|regex:(^[a-zA-Z ])',
            'apellido_trabajador' => 'required|max:255|regex:(^[a-zA-Z ])',
            'direccion' => 'required|max:255|regex:(^[A-Za-z0-9 .-])',
        ]);



        $trabajadores = new Trabajador();
        $trabajadores->create($request->all());

        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Trabajador registrado con exito',
            'code' => 'success',
        ];

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Registro de nuevo trabajador';
        $bitacoras->save();
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $trabajadores = Trabajador::all();
        foreach($trabajadores as $trabajador){

            $trabajador->botones =  sprintf(
                "  <button class='btn btn-warning btn-sm process' data-id='%s' data-toggle='modal' data-target='#modalShow'><i class='fa-solid fa-eye'></i> Detalles</button>",
                $trabajador->id
            );

            $trabajador->botones .=  sprintf(
                "  <button class='btn btn-light btn-sm process' data-id='%s'  data-toggle='modal' data-target='#modalEdit'><i class='fa-solid fa-pen-to-square'></i> Editar</button>", 
                $trabajador->id
            );

            $trabajador->botones .=  sprintf(
                "  <button class='btn btn-danger btn-sm delete' data-id='%s'><i class='fa-solid fa-trash-can'></i> Eliminar</button>",  
                $trabajador->id
            );
        }

        return $trabajadores;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validated = $request->validate([
            'dni' => 'required|max:255|regex:(^[0-9])',
            'telefono' => 'required|max:255|regex:(^[0-9])',
            'email' => 'required|max:255',
            'nombre_trabajador' => 'required|max:255|regex:(^[a-zA-Z ])',
            'cargo_trabajador' => 'required|max:255|regex:(^[a-zA-Z ])',
            'apellido_trabajador' => 'required|max:255|regex:(^[a-zA-Z ])',
            'direccion' => 'required|max:255|regex:(^[A-Za-z0-9 .-])',
        ]);

        $id=$request->id;
        $trabajador=Trabajador::find($id);
        $trabajador->update($request->all());
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Trabajador actualizado con exito',
            'code' => 'success',
        ];
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Actualización de trabajador';
        $bitacoras->save();
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id=$request->id;
        $trabajador=Trabajador::find($id);
        $trabajador->delete();
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Trabajador eliminado con exito',
            'code' => 'success',
        ];

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Eliminación de trabajador';
        $bitacoras->save();

        return $data;
    }

    public function find(Request $request) {

        $id=$request->id;
        $trabajador=\DB::table('trabajadors')->where('id', '=', $id)->get();
        return $trabajador[0];
    }
}
