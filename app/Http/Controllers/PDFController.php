<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\Producto;
use App\Models\Cliente;
use App\Models\Proveedor;
use App\Models\Trabajador;

class PDFController extends Controller
{
    public function productosPDF () {
        $productos = Producto::all();
        $pdf = PDF::loadView('archivos.PDFProductos',compact('productos'));
        return $pdf->download('Productos.pdf');
    }

    public function proveedoresPDF () {
        $proveedores = Proveedor::all();
        $pdf = PDF::loadView('archivos.PDFProveedor',compact('proveedores'));
        return $pdf->download('Proveedores.pdf');
    }

    public function clientesPDF () {
        $clientes = Cliente::all();
        $pdf = PDF::loadView('archivos.PDFCliente',compact('clientes'));
        return $pdf->download('Clientes.pdf');
    }

    public function trabajadoresPDF () {
        $trabajadores = Trabajador::all();
        $pdf = PDF::loadView('archivos.PDFTrabajador',compact('trabajadores'));
        return $pdf->download('Trabajadores.pdf');
    }

}



