<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Bitacora;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;


class UsuarioController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo usuario';
        $bitacoras->save();
        $roles = Role::all();
        return view('usuario.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuarios = new User();
        $usuarios->name = $request->get('name');
        $usuarios->email = $request->get('email');
        $usuarios->password = Hash::make($request->get('password'));
        $usuarios->id_tipo_usuario = $request->get('id_tipo_usuario');
        $usuarios->save();
        $usuarios->roles()->sync($request->get('permiso'));
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Usuario registrado con exito',
            'code' => 'success',
        ];
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Registro de nuevo usuario';
        $bitacoras->save();    
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $usuarios= DB::table('users')->join('tipo_usuario','users.id_tipo_usuario','=', 'tipo_usuario.id')
        ->select('users.*','tipo_usuario.tipo_usuario')->get();
        foreach($usuarios as $usuario) {

            $usuario->botones =  sprintf(
            " <button class='btn btn-warning btn-sm process' data-id='%s' data-toggle='modal' data-target='#modalShow'><i class='fa-solid fa-eye'></i> Detalles</button>",
            $usuario->id
        );

        $usuario->botones .= sprintf(
            "  <button class='btn btn-light btn-sm process' data-id='%s' data-toggle='modal' data-target='#modalEdit'><i class='fa-solid fa-pen-to-square'></i> Editar</button>",
            $usuario->id
        );

        $usuario->botones .= sprintf(
            "  <button class='btn btn-danger btn-sm delete' data-id='%s'><i class='fa-solid fa-trash-can'></i> Eliminar</button>",
            $usuario->id
        );
        }

        return $usuarios;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->id;
        $usuarios= User::find($id);
        $usuarios->name = $request->get('name');
        $usuarios->email = $request->get('email');
        $usuarios->password = Hash::make($request->get('password'));
        $usuarios->id_tipo_usuario = $request->get('id_tipo_usuario');
        $usuarios->save();
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Usuario actualizado con exito',
            'code' => 'success',
        ];
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Actualización de usuario';
        $bitacoras->save();
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id=$request->id;
        $usuarios=User::find($id);
        $usuarios->delete();

        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Usuario eliminado con exito',
            'code' => 'success',
        ];
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Eliminación de usuario';
        $bitacoras->save();
        return $data;
    }

    public function find(Request $request) {

        $id=$request->id;

        $usuarios= DB::table('users')->join('tipo_usuario','users.id_tipo_usuario','=', 'tipo_usuario.id')
        ->select('users.*','tipo_usuario.tipo_usuario')->where('users.id', '=', $id)->get();
        
        return $usuarios[0];
    }
}
