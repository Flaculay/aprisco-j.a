<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Trabajador;
use App\Models\Venta;
use App\Models\Cliente;
use App\Models\DetalleVenta;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use App\Models\Bitacora;
use Illuminate\Support\Facades\Auth;

class VentaController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ventas= DB::table('ventas')->join('clientes','ventas.id_cliente','=', 'clientes.id')
        ->join('trabajadors', 'ventas.id_trabajador','=', 'trabajadors.id')
        ->select('ventas.*','clientes.nombre_cliente','trabajadors.nombre_trabajador')->get();

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo ventas';
        $bitacoras->save();
        
        return view('ventas.index', compact('ventas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ventas= Venta::all();
        $productos= Producto::all();
        $clientes= Cliente::all();
        $trabajadores = Trabajador::all();

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo nueva venta';
        $bitacoras->save();

        return view('ventas.create', compact('productos', 'clientes', 'trabajadores', 'ventas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ventas = new Venta();
        $ventas->fecha_venta = $request->get('fecha');
        $ventas->id_cliente = $request->get('cliente');
        $ventas->id_trabajador = $request->get('trabajador');
        $ventas->total_pagar = $request->get('total');
        $ventas->metodo_pago = $request->get('metodo');
        $ventas->banco_pago = $request->get('banco');
        $ventas->save();

        foreach($request->id_producto as $producto => $productos) {
            $resultado[] = array('id_producto'=>$request->id_producto[$producto],
            'precio'=>$request->precio[$producto],'cantidad'=>$request->cantidad[$producto]);

            $id_producto = $request->id_producto[$producto];
            $cantidad = $request->cantidad[$producto];

            $sql = DB::update('update productos set cantidad = ( cantidad - '. $cantidad .' ) where id = '. $id_producto);
        }

        $ventas->detalleVentas()->createMany($resultado);

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Registro de venta';
        $bitacoras->save();

        return redirect('/ventas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ventas = Venta::find($id);
        $subtotal = 0 ;
        $detalleVentas = $ventas->detalleVentas;
        foreach ($detalleVentas as $detalleVenta) {
            $subtotal += $detalleVenta->cantidad * $detalleVenta->precio;
        }

        $pdf = PDF::loadView('archivos.PDFacturasVentas', compact('ventas', 'detalleVentas', 'subtotal'));
        return $pdf->download('Factura.pdf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ventas =Venta::find($id);
        $ventas->delete();

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Eliminación de venta';
        $bitacoras->save();

        return redirect('/ventas');
    }

    public function mostrar() {
        $ventas = Venta::whereDate('fecha_venta', Carbon::now('America/Caracas'))->get();
        $total = $ventas->sum('total_pagar');

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo de ventas por fecha';
        $bitacoras->save();

        return view ('reportes.ventas', compact('ventas', 'total'));
    }

    public function mostrardia() {
        $ventas = Venta::whereDate('fecha_venta', Carbon::now('America/Caracas'))->get();
        $total = $ventas->sum('total_pagar');

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo de ventas por día';
        $bitacoras->save();

        return view ('reportes.ventas_dia', compact('ventas', 'total'));
    }

    public function resultados_reporte (Request $request) {
        $fi = $request->get('fecha_ini');
        $ff = $request->get('fecha_fin');
        $ventas = Venta::whereBetween('fecha_venta', [$fi,$ff])->get();
        $total = $ventas->sum('total_pagar');
        return view ('reportes.ventas',compact('ventas', 'total'));
    }
}
