<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Bitacora;
use Illuminate\Support\Facades\Auth;

class CategoriasController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo categoria';
        $bitacoras->save();
        return view('categorias.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 
        $validated = $request->validate([
            'categoria_nombre' => 'required|unique:categorias|max:255|regex:(^[a-zA-Z ])'
        ]);

        $categorias = new Categoria();
        $categorias->create($request->all());

        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Categoria registrada con exito',
            'code' => 'success',
        ];
        
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Creación de nueva categoria';
        $bitacoras->save();
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   

        $categorias = Categoria::all();
        foreach($categorias as $categoria){

            $categoria->botones =  sprintf(
                "  <button class='btn btn-light btn-sm process' data-id='%s'  data-toggle='modal' data-target='#modalEdit'><i class='fa-solid fa-pen-to-square'></i> Editar</button>", 
                $categoria->id
            );

            $categoria->botones .=  sprintf(
                "  <button class='btn btn-danger btn-sm delete' data-id='%s'><i class='fa-solid fa-trash-can'></i> Eliminar</button>",  
                $categoria->id
            );
        }

        return $categorias;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validated = $request->validate([
            'categoria_nombre' => 'required|max:255|regex:(^[a-zA-Z ])'
        ]);

        $id=$request->id;
        $categoria=Categoria::find($id);
        $categoria->update($request->all());
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Categoria actualizada con exito',
            'code' => 'success',
        ];

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Modificación de categoria';
        $bitacoras->save();
        
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $id=$request->id;
        $categoria=Categoria::find($id);
        $categoria->delete();
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Proveedor eliminado con exito',
            'code' => 'success',
        ];

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Eliminación de categoria';
        $bitacoras->save();

        return $data;
    }

    public function find(Request $request) {

        $id=$request->id;
        $categorias=\DB::table('categorias')->where('id', '=', $id)->get();
        return $categorias[0];
    }
}
