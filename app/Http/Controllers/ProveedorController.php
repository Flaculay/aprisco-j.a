<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proveedor;
use App\Models\Bitacora;
use Illuminate\Support\Facades\Auth;

class ProveedorController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo proveedor';
        $bitacoras->save();
        return view('proveedor.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'nombre_proveedor' => 'required|unique:proveedors|max:255|regex:(^[A-Za-z0-9 .-])',
            'dni' => 'required|unique:proveedors|max:255|regex:(^[0-9])',
            'email' => 'required|unique:proveedors|max:255',
            'telefono' => 'required|unique:proveedors|max:255|regex:(^[0-9])',
            'nombre_vendedor' => 'max:255|regex:(^[A-Za-z ])',
            'direccion_fiscal' => 'required|max:255|regex:(^[A-Za-z0-9 .-])',
        ]);
        
        $proveedores = new Proveedor();
        $proveedores->create($request->all());

        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'proveedor registrado con exito',
            'code' => 'success',
        ];$bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Registro de nuevo proveedor';
        $bitacoras->save();
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   

        $proveedores = Proveedor::all();
        foreach($proveedores as $proveedor){

            $proveedor->botones =  sprintf(
                "  <button class='btn btn-warning btn-sm process' data-id='%s' data-toggle='modal' data-target='#modalShow'><i class='fa-solid fa-eye'></i> Detalles</button>",
                $proveedor->id
            );

            $proveedor->botones .=  sprintf(
                "  <button class='btn btn-light btn-sm process' data-id='%s'  data-toggle='modal' data-target='#modalEdit'><i class='fa-solid fa-pen-to-square'></i> Editar</button>", 
                $proveedor->id
            );

            $proveedor->botones .=  sprintf(
                "  <button class='btn btn-danger btn-sm delete' data-id='%s'><i class='fa-solid fa-trash-can'></i> Eliminar</button>",  
                $proveedor->id
            );
        }

        return $proveedores;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validated = $request->validate([
            'nombre_proveedor' => 'required|max:255|regex:(^[A-Za-z0-9 .-])',
            'dni' => 'required|max:255|regex:(^[0-9])',
            'email' => 'required|max:255',  
            'telefono' => 'required|max:255|regex:(^[0-9])',
            'nombre_vendedor' => 'max:255|regex:(^[A-Za-z ])',
            'direccion_fiscal' => 'required|max:255|regex:(^[A-Za-z0-9 .-])',
        ]);

        $id=$request->id;
        $proveedor=Proveedor::find($id);
        $proveedor->update($request->all());
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Proveedor actualizado con exito',
            'code' => 'success',
        ];
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Actualización de proveedor';
        $bitacoras->save();
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $id=$request->id;
        $proveedor=Proveedor::find($id);
        $proveedor->delete();
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Proveedor eliminado con exito',
            'code' => 'success',
        ];
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Eliminación de proveedor';
        $bitacoras->save();
        return $data;
    }

    public function find(Request $request) {

        $id=$request->id;
        $productos=\DB::table('proveedors')->where('id', '=', $id)->get();
        return $productos[0];
    }
}
