<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Proveedor;
use App\Models\Compra;
use App\Models\DetalleCompra;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use App\Models\Bitacora;
use Illuminate\Support\Facades\Auth;

class CompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }


    public function index()
    {
        $compras= DB::table('compras')->join('proveedors','compras.id_proveedor','=', 'proveedors.id')
        ->select('compras.*','proveedors.nombre_proveedor')->get();

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo compras';
        $bitacoras->save();

        return view('compras.index', compact('compras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productos= Producto::all();
        $proveedores= Proveedor::all();


        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo nueva compra';
        $bitacoras->save();

        return view('compras.create', compact('productos', 'proveedores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $compras = new Compra();
        $compras->fecha_compra = $request->get('fecha');
        $compras->id_proveedor = $request->get('proveedor');
        $compras->total_pagar = $request->get('total');
        $compras->save();

        foreach($request->id_producto as $producto => $productos) {
            $resultado[] = array('id_producto'=>$request->id_producto[$producto],
            'cantidad'=>$request->cantidad[$producto],'costo'=>$request->costo[$producto],
            'precio'=>$request->precio[$producto]);

            $id_producto = $request->id_producto[$producto];
            $cantidad = $request->cantidad[$producto];
            $precio = $request->precio[$producto];

            $sql = DB::update('update productos set cantidad = ( cantidad + '. $cantidad .' ) , precio = '. $precio .' where id = '. $id_producto);

        }
        $compras->DetalleCompras()->createMany($resultado);

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Registro de compra';
        $bitacoras->save();

        return redirect('/compras');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $compras = Compra::find($id);
        $subtotal = 0 ;
        $detalleCompras = $compras->detalleCompras;
        foreach ($detalleCompras as $detalleCompra) {
            $subtotal += $detalleCompra->cantidad * $detalleCompra->costo;
        }

        $pdf = PDF::loadView('archivos.PDFacturasCompras', compact('compras', 'detalleCompras', 'subtotal'));
        return $pdf->download('Factura.pdf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $compra =Compra::find($id);
        $compra->delete();

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Eliminación de compra';
        $bitacoras->save();

        return redirect('/compras');
    }

    public function find(Request $request) {

    }

    public function mostrar() {
        $compras = Compra::whereDate('fecha_compra', Carbon::now('America/Caracas'))->get();
        $total = $compras->sum('total_pagar');

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo de compras por fecha';
        $bitacoras->save();

            return view ('reportes.compras',compact('compras', 'total'));
    }

    public function mostrardia() {
        $compras = Compra::whereDate('fecha_compra', Carbon::now('America/Caracas'))->get();
        $total = $compras->sum('total_pagar');

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo de compras por día';
        $bitacoras->save();
        
        return view ('reportes.compras_dia', compact('compras', 'total'));
    }

    public function resultados_reporte (Request $request) {
        $fi = $request->get('fecha_ini');
        $ff = $request->get('fecha_fin');
        $compras = Compra::whereBetween('fecha_compra', [$fi,$ff])->get();
        $total = $compras->sum('total_pagar');
        return view ('reportes.compras',compact('compras', 'total'));
    }

}
