<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Medida;
use App\Models\Bitacora;
use Illuminate\Support\Facades\Auth;

class ProductoController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Ingreso a módulo producto';
        $bitacoras->save();

        $categorias= Categoria::all();
        return view('producto.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated = $request->validate([
            'codigo' => 'required|unique:productos|max:255|regex:(^[A-Za-z0-9 .-])',
            'nombre' => 'required|unique:productos|max:255|regex:(^[A-Za-z0-9 .-])',
            'cantidad' => 'required|max:255|regex:(^[0-9])',
            'precio' => 'required|max:255|regex:(^[0-9])',
            'descripcion' => 'required|max:255|regex:(^[A-Za-z0-9 .-])',
        ]);

        $productos = new Producto();
        $productos->create($request->all());

        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Producto registrado con exito',
            'code' => 'success',
        ];
        
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Registro de nuevo producto';
        $bitacoras->save();

        return $data;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   
        $productos= DB::table('productos')->join('categorias','productos.id_tipo_producto','=', 'categorias.id')
        ->join('medida','productos.id_medida','=', 'medida.id')
        ->select('productos.*','categorias.categoria_nombre','medida.nombre_medida')->get();
        foreach($productos as $producto) {

            $producto->botones =  sprintf(
            " <button class='btn btn-warning btn-sm process' data-id='%s' data-toggle='modal' data-target='#modalShow'><i class='fa-solid fa-eye'></i> Detalles</button>",
            $producto->id
        );

        $producto->botones .= sprintf(
            "  <button class='btn btn-light btn-sm process' data-id='%s' data-toggle='modal' data-target='#modalEdit'><i class='fa-solid fa-pen-to-square'></i> Editar</button>",
            $producto->id
        );

        $producto->botones .= sprintf(
            "  <button class='btn btn-danger btn-sm delete' data-id='%s'><i class='fa-solid fa-trash-can'></i> Eliminar</button>",
            $producto->id
        );
        }

        return $productos;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validated = $request->validate([
            'codigo' => 'required|max:255|regex:(^[A-Za-z0-9 .-])',
            'nombre' => 'required|max:255|regex:(^[A-Za-z0-9 .-])',
            'cantidad' => 'required|max:255|regex:(^[0-9])',
            'precio' => 'required|max:255|regex:(^[0-9])',
            'descripcion' => 'required|max:255|regex:(^[A-Za-z0-9 .-])',
        ]);


        $id=$request->id;
        $productos= Producto::find($id);
        $productos->update($request->all());
        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Producto actualizado con exito',
            'code' => 'success',
        ];

        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Actualización se producto';
        $bitacoras->save();
        
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $id=$request->id;
        $productos=Producto::find($id);
        $productos->delete();

        $data= [
            'tittle' => 'Buen trabajo',
            'msg' => 'Producto eliminado con exito',
            'code' => 'success',
        ];
        
        $bitacoras = new Bitacora();
        $bitacoras->id_usuario=Auth::user()->id;
        $bitacoras->accion= 'Eliminación de producto';
        $bitacoras->save();

        return $data;
    }

        public function find(Request $request) {

        $id=$request->id;

        $productos= DB::table('productos')->join('categorias','productos.id_tipo_producto','=', 'categorias.id')
        ->join('medida','productos.id_medida','=', 'medida.id')
        ->select('productos.*','categorias.categoria_nombre','medida.nombre_medida')->where('productos.id', '=', $id)->get();
        
        return $productos[0];
    }

    public function obtenerProductos(Request $request){
        if ($request->ajax()){
            $productos = Producto::find($request->id_producto);
            return response()->json($productos);
        }
    }
}
