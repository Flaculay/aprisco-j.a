<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre_cliente',
        'apellido_cliente',
        'tipo_documento',
        'dni',
        'telefono',
        'email',
        'direccion',
    ];
}
