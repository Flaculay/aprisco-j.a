<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    public function categorias() {
        return $this->belongsTo(Categoria::class, 'id_tipo_producto');
    }

    public function medidas() {
        return $this->belongsTo(Categoria::class, 'id_medida');
    }
    

    protected $fillable = [
        'codigo',
        'nombre',
        'cantidad',
        'precio',
        'descripcion',
        'id_tipo_producto',
        'id_medida',
    ];
}
