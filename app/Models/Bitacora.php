<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_usuario',
        'accion',
    ];

    public function usuario() {
        return $this->belongsTo(User::class, 'id_usuario');
    }
}
