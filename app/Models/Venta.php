<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    use HasFactory;

    protected $fillable = [
        'fecha_venta',
        'total_pagar',
        'id_cliente',
        'id_trabajador',
        'metodo_pago',
        'banco_pago',
    ];

    public function trabajadors() {
        return $this->belongsTo(Trabajador::class , 'id_trabajador');
    }

    public function cliente() {
        return $this->belongsTo(Cliente::class, 'id_cliente');
    }

    public function detalleVentas() {
        return $this->hasMany(DetalleVenta::class);
    }
}
