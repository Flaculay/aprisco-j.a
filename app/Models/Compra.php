<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    use HasFactory;

    protected $fillable = [
        'fecha_compra',
        'total_pagar',
        'id_proveedor',
    ];

    public function proveedor() {
        return $this->belongsTo(Proveedor::class, 'id_proveedor');
    }

    public function DetalleCompras() {
        return $this->hasMany(DetalleCompra::class);
    }
}
