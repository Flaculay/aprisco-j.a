<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleCompra extends Model
{
    use HasFactory;

    protected $fillable = [
        'cantidad',
        'costo',
        'precio',
        'compra_id',
        'id_producto',
    ];

    public function compra() {
        return $this->belongsTo(Compra::class);
    }

    public function producto() {
        return $this->belongsTo(Producto::class, 'id_producto');
    }
}
