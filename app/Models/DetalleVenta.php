<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleVenta extends Model
{
    use HasFactory;

    protected $fillable = [
        'cantidad',
        'precio',
        'venta_id',
        'id_producto',
    ];

    public function compra() {
        return $this->belongsTo(Venta::class);
    }

    public function producto() {
        return $this->belongsTo(Producto::class, 'id_producto');
    }
}
