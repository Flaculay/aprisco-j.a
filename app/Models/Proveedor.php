<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre_proveedor',
        'tipo_documento',
        'dni',
        'email',
        'telefono',
        'nombre_vendedor',
        'direccion_fiscal',
    ];
}
