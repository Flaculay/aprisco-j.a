<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre_trabajador',
        'apellido_trabajador',
        'tipo_documento',
        'dni',
        'cargo_trabajador',
        'email',
        'telefono',
        'fecha_ingreso',
        'direccion',
        
    ];
}
