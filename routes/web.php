<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\ProveedorController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\CategoriasController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\TrabajadorController;
use App\Http\Controllers\BitacoraController;
use App\Http\Controllers\CompraController;
use App\Http\Controllers\VentaController;
use App\Http\Controllers\ReportesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/main', function () {
    return view('main');
});

Route::resource('productos', ProductoController::class);
Route::resource('proveedores', ProveedorController::class);
Route::resource('clientes', ClienteController::class);
Route::resource('usuarios', UsuarioController::class);
Route::resource('categorias', CategoriasController::class);
Route::resource('trabajador', TrabajadorController::class);
Route::resource('bitacora', BitacoraController::class);
Route::resource('compras', CompraController::class);
Route::resource('ventas', VentaController::class);
Route::resource('reportes', ReportesController::class);

Route::get('comprasData', [CompraController::class, 'show']);
Route::post('comprasfind', [CompraController::class, 'find']);

Route::get('bitacoraData', [BitacoraController::class, 'show']);

Route::get('productoData', [ProductoController::class, 'show']);
Route::post('productosupdate', [ProductoController::class, 'update']);
Route::post('productosfind', [ProductoController::class, 'find']);
Route::post('productosCreate', [ProductoController::class, 'store']);
Route::post('productosDestroy', [ProductoController::class, 'destroy']);

Route::get('proveedorData', [ProveedorController::class, 'show']);
Route::post('proveedorupdate', [ProveedorController::class, 'update']);
Route::post('proveedorfind', [ProveedorController::class, 'find']);
Route::post('proveedorCreate', [ProveedorController::class, 'store']);
Route::post('proveedorDestroy', [ProveedorController::class, 'destroy']);

Route::get('clienteData', [ClienteController::class, 'show']);
Route::post('clienteupdate', [ClienteController::class, 'update']);
Route::post('clientefind', [ClienteController::class, 'find']);
Route::post('clienteCreate', [ClienteController::class, 'store']);
Route::post('clienteDestroy', [ClienteController::class, 'destroy']);

Route::get('usuariosData', [UsuarioController::class, 'show']);
Route::post('usuariosupdate', [UsuarioController::class, 'update']);
Route::post('usuariosfind', [UsuarioController::class, 'find']);
Route::post('usuariosCreate', [UsuarioController::class, 'store']);
Route::post('usuariosDestroy', [UsuarioController::class, 'destroy']);

Route::get('categoriasData', [CategoriasController::class, 'show']);
Route::post('categoriasupdate', [CategoriasController::class, 'update']);
Route::post('categoriasfind', [CategoriasController::class, 'find']);
Route::post('categoriasCreate', [CategoriasController::class, 'store']);
Route::post('categoriasDestroy', [CategoriasController::class, 'destroy']);

Route::get('trabajadorData', [TrabajadorController::class, 'show']);
Route::post('trabajadorupdate', [TrabajadorController::class, 'update']);
Route::post('trabajadorfind', [TrabajadorController::class, 'find']);
Route::post('trabajadorCreate', [TrabajadorController::class, 'store']);
Route::post('trabajadorDestroy', [TrabajadorController::class, 'destroy']);

Route::get('obtener_productos', [ProductoController::class, 'obtenerProductos'])->name('obtener_productos');
Route::get('reportes_compras', 'App\Http\Controllers\CompraController@mostrar')->name('reportes_compras');
Route::get('reportes_compras_dia', 'App\Http\Controllers\CompraController@mostrardia')->name('reportes_compras_dia');
Route::get('reportes_ventas', 'App\Http\Controllers\VentaController@mostrar')->name('reportes_ventas');;
Route::get('reportes_ventas_dia', 'App\Http\Controllers\VentaController@mostrardia')->name('reportes_ventas_dia');
Route::post('resultados_compra', 'App\Http\Controllers\CompraController@resultados_reporte')->name('resultados_compra');
Route::post('resultados_venta', 'App\Http\Controllers\VentaController@resultados_reporte')->name('resultados_venta');

Route::get('PDFproductos', [PDFController::class, 'productosPDF'])->name('PDFProductos');
Route::get('PDFProveedores', [PDFController::class, 'proveedoresPDF'])->name('PDFProveedores');
Route::get('PDFClientes', [PDFController::class, 'clientesPDF'])->name('PDFClientes');
Route::get('PDFTrabajadores', [PDFController::class, 'trabajadoresPDF'])->name('PDFTrabajadores');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
