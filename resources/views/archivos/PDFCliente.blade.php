<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clientes</title>
    <link rel="stylesheet" type="text/css" href="{{ public_path('css/app.css') }}">
  </head>
  <body>
  <h1 style="text-align:center; background-color:#892A38;color:snow;border:3px;border-radius:10px;">Clientes</h1>
<table class="table table-striped table-bordered " style="width: 100%;">
<thead class="thead">
  <tr style="text-align:center;width:150px;background-color:#892A38;color:snow;border:3px;border-radius:10px;" >
      <th>Nombre</th>
      <th>Apellido</th>
      <th>Número de documento</th>
      <th>telefono</th>
      <th>Correo</th>
      <th>Dirección</th>
  </tr>
</thead>
<tbody>
  @foreach($clientes as $cliente)
  <tr>
      <td>{{$cliente->nombre_cliente}}</td>
      <td>{{$cliente->apellido_cliente}}</td>
      <td>{{$cliente->dni}}</td>
      <td>{{$cliente->telefono}}</td>
      <td>{{$cliente->email}}</td>
      <td>{{$cliente->direccion}}</td>
  </tr>
  @endforeach
</tbody>
</table>
  </body>
</html>