<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Productos</title>
    <link rel="stylesheet" type="text/css" href="{{ public_path('css/app.css') }}">
  </head>
  <body>
  <h1 style="text-align:center; background-color:#892A38;color:snow;border:3px;border-radius:10px;">Productos</h1>
<table class="table table-striped table-bordered " style="width: 100%;">
<thead class="thead">
  <tr style="text-align:center;width:150px;background-color:#892A38;color:snow;border:3px;border-radius:10px;">
      <th>ID</th>
      <th>Código</th>
      <th>Nombre</th>
      <th>Cantidad</th>
      <th>Precio ($)</th>
      <th>Categoria</th>
      <th>Descripción</th>
  </tr>
</thead>
<tbody>
  @foreach($productos as $producto)
  <tr>
      <td>{{$producto->id}}</td>
      <td>{{$producto->codigo}}</td>
      <td>{{$producto->nombre}}</td>
      <td>{{$producto->cantidad}}</td>
      <td>{{$producto->precio}}</td>
      <td>{{$producto->categorias->categoria_nombre}}</td>
      <td>{{$producto->descripcion}}</td>
  </tr>
  @endforeach
</tbody>
</table>
  </body>
</html>