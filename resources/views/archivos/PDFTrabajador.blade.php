<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trabajadores</title>
    <link rel="stylesheet" type="text/css" href="{{ public_path('css/app.css') }}">
  </head>
  <body>
  <h1 style="text-align:center; background-color:#892A38;color:snow;border:3px;border-radius:10px;">Trabajadores</h1>
<table class="table table-striped table-bordered " style="width: 100%;">
<thead class="thead">
  <tr style="text-align:center;width:150px;background-color:#892A38;color:snow;border:3px;border-radius:10px;">
      <th>Nombre</th>
      <th>Apellido</th>
      <th>Número de documento</th>
      <th>Cargo</th>
      <th>Correo</th>
      <th>Teléfono</th>
      <th>Fecha de ingreso</th>
      <th>Dirección</th>
  </tr>
</thead>
<tbody>
  @foreach($trabajadores as $trabajador)
  <tr>
      <td>{{$trabajador->nombre_trabajador}}</td>
      <td>{{$trabajador->apellido_trabajador}}</td>
      <td>{{$trabajador->dni}}</td>
      <td>{{$trabajador->cargo_trabajador}}</td>
      <td>{{$trabajador->email}}</td>
      <td>{{$trabajador->telefono}}</td>
      <td>{{\Carbon\Carbon::parse($trabajador->fecha_ingreso)->format('d, M  Y')}}</td>
      <td>{{$trabajador->direccion}}</td>
  </tr>
  @endforeach
</tbody>
</table>
  </body>
</html>