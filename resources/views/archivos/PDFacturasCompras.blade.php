<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Compras</title>
</head>
<body>

<img style="width: 150px" src="logo.jpg" alt="">

<h1 style="text-align:center; background-color:#892A38;color:snow;border:3px;border-radius:10px;">Compras</h1>
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-4 text-center">
                            <h3 class="form-control-h3" style="text-align:center;width:100px;background-color:#892A38;color:snow;border:3px;border-radius:10px;"><strong>Proveedor</strong></h3>
                            <p>
                                {{$compras->proveedor->nombre_proveedor}}
                            </p>
                        </div>
                        <div class="col-md-4 text-center">
                            <h4 class="form-control-h4" style="text-align:center;width:150px;background-color:#892A38;color:snow;border:3px;border-radius:10px;"><strong>Número de factura</strong></h4>
                            <p>{{$compras->id}}</p>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <h4 class="card-title" style="text-align:center;width:150px;background-color:#892A38;color:snow;border:3px;border-radius:10px;">Detalles de compra</h4>
                        <div class="table-responsive col-md-12">
                            <table id="saleDetails" class="table" style="width: 100%;">
                                <thead>
                                    <tr style="text-align:center;width:150px;background-color:#892A38;color:snow;border:3px;border-radius:10px;">
                                        <th>Producto</th>
                                        <th>Precio de compra (USD)</th>
                                        <th>Cantidad</th>
                                        <th>SubTotal(USD)</th>
                                    </tr>
                                </thead>
                                <tfoot>

                                <tbody>
                                    @foreach($detalleCompras as $detalleCompra)
                                    <tr>
                                        <td>{{$detalleCompra->producto->nombre}}</td>
                                        <td>USD {{$detalleCompra->costo}}</td>
                                        <td>{{$detalleCompra->cantidad}}</td>
                                        <td>USD {{number_format($detalleCompra->cantidad*$detalleCompra->costo)}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>

                                    <tr>
                                        <th colspan="4">
                                            <p align="right">SUBTOTAL:</p>
                                        </th>
                                        <th>
                                            <p align="right">USD {{number_format($subtotal,2)}}</p>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="4">
                                            <p align="right">TOTAL:</p>
                                        </th>
                                        <th>
                                            <p align="right">USD {{number_format($subtotal,2)}}</p>
                                        </th>
                                    </tr>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
       
    
</body>
</html>