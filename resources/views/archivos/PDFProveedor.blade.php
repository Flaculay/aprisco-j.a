<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Proveedores</title>
    <link rel="stylesheet" type="text/css" href="{{ public_path('css/app.css') }}">
  </head>
  <body>
  <h1 style="text-align:center; background-color:#892A38;color:snow;border:3px;border-radius:10px;">Proveedores</h1>
<table class="table table-striped table-bordered " style="width: 100%;">
<thead class="thead">
  <tr style="text-align:center;width:150px;background-color:#892A38;color:snow;border:3px;border-radius:10px;">
      <th>Nombre</th>
      <th>Número de documento</th>
      <th>Correo</th>
      <th>Telefono</th>
      <th>Nombre del vendedor</th>
      <th>Dirección</th>
  </tr>
</thead>
<tbody>
  @foreach($proveedores as $proveedor)
  <tr>
      <td>{{$proveedor->nombre_proveedor}}</td>
      <td>{{$proveedor->dni}}</td>
      <td>{{$proveedor->email}}</td>
      <td>{{$proveedor->telefono}}</td>
      <td>{{$proveedor->nombre_vendedor ?? 'No aplica'}}</td>
      <td>{{$proveedor->direccion_fiscal}}</td>
  </tr>
  @endforeach
</tbody>
</table>
  </body>
</html>