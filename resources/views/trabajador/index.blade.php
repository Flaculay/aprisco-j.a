@extends('main')

@section('title', 'Trabajadores')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Trabajadores') }}
        </h2>
        <input type="hidden" value="{{ csrf_token() }}" id="token">


<a href="{{route('PDFTrabajadores')}}" class="btn btn-danger float-right"><i class="fa-solid fa-file-pdf"></i> Generar PDF</a>
<a data-toggle="modal" data-target="#modalCreate" class="btn btn-primary modCreate"><i class="fa-solid fa-plus"></i> Nuevo</a>

<br><br>
<div class ="card">
    <div class ="card-body">
    <table id="trabajadores" class="table table-hover table-striped table-bordered table-sm mt-4" style="width:100%">
  <thead>
      <tr >
          <th scope="col">ID</th>
          <th scope="col">Nombre</th>
          <th scope="col">Apellido</th>
          <th scope="col">Cargo</th>
          <th scope="col">Correo</th>
          <th scope="col">Telefono</th>
          <th scope="col">Dirección</th>
          <th style="width:250px" scope="col">Acciones</th>
      </tr>
  </thead>
  <tbody>
  </tbody>
</table>
    </div>
</div>

@include('modals.trabajador.index')
@endsection
@section('js')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function() {

        trabajadores = $('#trabajadores').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por paginas",
            "zeroRecords": "No se encontraron registros",
            "info": "Mostrando  _END_ registros de un total de _TOTAL_ registros",
             
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "No se encontraron registros",
            "search": "Buscar",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",

                "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },

        'ajax':{
            'url':'trabajadorData',
            'dataSrc':''
        },
        'columns':[
            {'data':'id'},
            {'data':'nombre_trabajador'},
            {'data':'apellido_trabajador'},
            {'data':'cargo_trabajador'},
            {'data':'email'},
            {'data':'telefono'},
            {'data':'direccion'},
            {'data': 'botones'},
        ]
    });


        //---JS para obtener datos---//
        $(document).on('click', '.process', function () {

            var id= $(this).data('id');
            var token = $("#token").val();
                $.ajax({
                url:'trabajadorfind',
                headers:{'X-CSRF-TOKEN': token},
                method:'post',
                dataType:'json',
                data: {
                    id: id,
                }
            }).done(function(trabajador) {
                $("#idShow").val(trabajador.id);
                $("#nombreShow").val(trabajador.nombre_trabajador);
                $("#apellidoShow").val(trabajador.apellido_trabajador);
                $("#tipoShow").val(trabajador.tipo_documento);
                $("#dniShow").val(trabajador.dni);
                $("#cargoShow").val(trabajador.cargo_trabajador);
                $("#emailShow").val(trabajador.email);
                $("#telefonoShow").val(trabajador.telefono);
                $("#direccionShow").val(trabajador.direccion);
                $("#fechaShow").val(trabajador.fecha_ingreso);

                $("#idEdit").val(trabajador.id);
                $("#nombreEdit").val(trabajador.nombre_trabajador);
                $("#apellidoEdit").val(trabajador.apellido_trabajador);
                $("#tipoEdit").val(trabajador.tipo_documento);
                $("#dniEdit").val(trabajador.dni);
                $("#cargoEdit").val(trabajador.cargo_trabajador);
                $("#emailEdit").val(trabajador.email);
                $("#telefonoEdit").val(trabajador.telefono);
                $("#direccionEdit").val(trabajador.direccion);
                $("#fechaEdit").val(trabajador.fecha_ingreso);
                
            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'No se pudo obtener los datos del trabajador',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        });

        
        //---JS para eliminar---//

        $(document).on('click', '.delete', function () {

            Swal.fire({
                title: '¿Deseas eliminar este trabajador?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                    var id= $(this).data('id');
                    var token = $("#token").val();
                    $.ajax({
                    url:'trabajadorDestroy',
                    headers:{'X-CSRF-TOKEN': token},
                    method:'post',
                    dataType:'json',
                    data: {
                    id: id,
                    }
            }).done(function(trabajador) {
                
                 Swal.fire(
                    trabajador.tittle,
                    trabajador.msg,
                    trabajador.code
                )

                setTimeout( function () {
                    trabajadores.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El trabajador no ha podido ser eliminado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
        
        //---JS para el EDITAR---//
        $(document).on('click','#btnEdit', function () {
            var id= $("#idEdit").val();
            var nombre_trabajador= $("#nombreEdit").val();
            var apellido_trabajador= $("#apellidoEdit").val();
            var tipo_documento= $("#tipoEdit").val();
            var dni= $("#dniEdit").val();
            var cargo_trabajador= $("#cargoEdit").val();
            var email= $("#emailEdit").val();
            var telefono= $("#telefonoEdit").val();
            var fecha_ingreso= $("#fechaEdit").val();
            var direccion= $("#direccionEdit").val();

            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas actualizar este trabajador?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Actualizar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'trabajadorupdate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    id:id,
                    nombre_trabajador:nombre_trabajador,
                    apellido_trabajador:apellido_trabajador,
                    tipo_documento:tipo_documento,
                    dni:dni,
                    cargo_trabajador:cargo_trabajador,
                    email:email,
                    telefono:telefono,
                    fecha_ingreso:fecha_ingreso,
                    direccion:direccion,
                }
            }).done(function(trabajador) {
                
                Swal.fire(
                    trabajador.tittle,
                    trabajador.msg,
                    trabajador.code
                )

                setTimeout( function () {
                    $('#modalEdit').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    trabajadores.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El trabajador no ha podido ser actualizado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });

        //---JS PARA CREAR---//
        $(document).on('click', '#btnCreate', function () {
            var nombre_trabajador= $("#nombreCreate").val();
            var apellido_trabajador= $("#apellidoCreate").val();
            var tipo_documento= $("#tipoCreate").val();
            var dni= $("#dniCreate").val();
            var cargo_trabajador= $("#cargoCreate").val();
            var email= $("#emailCreate").val();
            var telefono= $("#telefonoCreate").val();
            var fecha_ingreso= $("#fechaCreate").val();
            var direccion= $("#direccionCreate").val();
            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas registrar este trabajador?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'agregar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'trabajadorCreate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    nombre_trabajador:nombre_trabajador,
                    apellido_trabajador:apellido_trabajador,
                    tipo_documento:tipo_documento,
                    dni:dni,
                    cargo_trabajador:cargo_trabajador,
                    email:email,
                    telefono:telefono,
                    fecha_ingreso:fecha_ingreso,
                    direccion:direccion,
                }
            }).done(function(proveedor) {
                
                Swal.fire(
                proveedor.tittle,
                proveedor.msg,
                proveedor.code
                )

                setTimeout( function () {
                    $('#modalCreate').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    trabajadores.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El proveedor no pudo ser registrado!',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
});


</script>

@endsection