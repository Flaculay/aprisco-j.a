@extends('main')

@section('title', 'Reportes')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
@endsection

@section('content')

<div class="row row-cols-1 row-cols-md-2 g-4">
  <div class="col">
    <div class="card">
      <img src="compras.jpeg" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">Reportes de compras por fecha</h5>
        <p class="card-text">Acá podrá ver las compras en un rango de fecha</p>
        <a href="reportes_compras" class="btn btn-primary">Ir a reportes</a>  
    </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <img src="compras_dia.jpeg" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">Reportes de compras por día</h5>
        <p class="card-text">Acá podrá ver las compras registradas el día de hoy</p>
        <a href="reportes_compras_dia" class="btn btn-primary">Ir a reportes</a>  
    </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <img src="ventas.jpeg" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">Reportes de venta por fecha</h5>
        <p class="card-text">Acá podrá ver las ventas en un rango de fecha</p>
        <a href="reportes_ventas" class="btn btn-primary">Ir a reportes</a>  
    </div>
    </div>
  </div>
  <div class="col">
    <div class="card">
      <img src="ventas_dia.jpeg" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">Reportes de venta por día</h5>
        <p class="card-text">Acá podrá ver las ventas registradas el día de hoy</p>
        <a href="reportes_ventas_dia" class="btn btn-primary">Ir a reportes</a>  
    </div>
    </div>
  </div>
</div>

@endsection
@section('js')


@endsection