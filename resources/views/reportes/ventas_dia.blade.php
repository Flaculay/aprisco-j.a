@extends('main')

@section('title', 'Ventas')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Reporte de ventas por día') }}
        </h2>
        <input type="hidden" value="{{ csrf_token() }}" id="token">

<br><br>
<div class ="card">
    <div class ="card-body">

                     <div class="row ">
                        <div class="col-12 col-md-4 text-center">
                            <span>Fecha de consulta: <b> </b></span>
                            <div class="form-group">
                                <strong>{{\Carbon\Carbon::now('America/Caracas')->format('d/m/Y')}}</strong>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 text-center">
                            <span>Cantidad de registros: <b></b></span>
                            <div class="form-group">
                                <strong>{{$ventas->count()}}</strong>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 text-center">
                            <span>Total de ingresos: <b> </b></span>
                            <div class="form-group">
                                <strong>usd {{$total}}</strong>
                            </div>
                        </div>
                    </div>

                    


        <table id="proveedores" class="table table-hover table-striped table-bordered table-sm mt-4" style="width:100%">
                        <thead >
                            <tr >
                                <th style="width:50px" scope="col">Número de factura</th>
                                <th style="width:100px" scope="col">Cliente</th>
                                <th style="width:100px" scope="col">Fecha</th>
                                <th style="width:100px" scope="col">Total</th>
                                <th style="width:100px" scope="col">Trabajador</th>
                                <th style="width:100px" scope="col">Acciones</th>
                            </tr>
                        </thead>
                    <tbody>
                    @foreach($ventas as $venta)
                <tr>
                <td>{{$venta->id}}</td>
                <td>{{$venta->nombre_cliente}}</td>
                <td>{{$venta->fecha_venta}}</td>
                <td>{{$venta->total_pagar}}</td>
                <td>{{$venta->nombre_trabajador}}</td>
                <td>
                    <a href="{{route('ventas.show', $venta->id)}}" class="btn btn-light">PDF</a>
                </td>
            </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@endsection
@section('js')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function() {

        proveedores = $('#proveedores').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por paginas",
            "zeroRecords": "No se encontraron registros",
            "info": "Mostrando  _END_ registros de un total de _TOTAL_ registros",
             
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "No se encontraron registros",
            "search": "Buscar",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",

                "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },
    });




});
</script>

@endsection