@extends('main')

@section('title', 'BItacora')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Bitacora') }}
        </h2>
        <input type="hidden" value="{{ csrf_token() }}" id="token">
<br><br>
<div style="width:800px" class="card">
    <div class ="card-body">
    <table id="bitacora" class="table table-hover table-striped table-bordered table-sm mt-4" style="width:100%">
  <thead>
      <tr >
          <th scope="col">ID</th>
          <th scope="col">Usuario</th>
          <th scope="col">Fecha</th>
          <th scope="col">Acción</th>
      </tr>
  </thead>
  <tbody>
  @foreach($bitacoras as $bitacora)
      <tr>
                <td>{{$bitacora->id}}</td>
                <td>{{$bitacora->usuario->name}}</td>
                <td>{{\Carbon\Carbon::parse($bitacora->created_at)->format('d, M  Y H:i:s')}}</td>
                <td>{{$bitacora->accion}}</td>
            </tr>
      @endforeach
  </tbody>
</table>
    </div>
</div>

@endsection
@section('js')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function() {

        proveedores = $('#bitacora').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por paginas",
            "zeroRecords": "No se encontraron registros",
            "info": "Mostrando  _END_ registros de un total de _TOTAL_ registros",
             
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "No se encontraron registros",
            "search": "Buscar",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",

                "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },
    });
});
</script>
@endsection