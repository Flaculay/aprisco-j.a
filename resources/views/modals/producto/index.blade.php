<!-- Modal Edit-->
<div class="modal fade" id="modalEdit" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Editar producto</h3>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
                <div class="modal-body">
                <form data-action="" method="POST">
                        <input name="id" id="id" type="hidden" value="">
                        <div class="mb-3">
                        <label for="" class="form-label">Código</label>
                        <input pattern="[A-Za-z0-9 ]+" id="codigo" name="codigo" type="text" class="form-control" value="">
                        </div>
                        <div class="mb-3">
                        <label for="" class="form-label">Nombre</label>
                        <input pattern="[A-Za-z ]+" id="nombre" name="nombre" type="text" class="form-control" value="">
                        </div>
                        <div class="mb-3">
                        <label for="" class="form-label">Cantidad</label>
                        <input pattern="[0-9]+" id="cantidad" name="cantidad" type="number" class="form-control" value="">
                        </div>
                        <div class="mb-3">
                        <label for="" class="form-label">Medida</label>
                        <select id="medida" name="medida" class="form-control">
                        <option disabled selected value="">-- Escoja la medida del producto --</option>
                        <option  value="1">L.</option>
                        <option  value="2">Kg.</option>
                        </select>
                        </div>
                        <div class="mb-3">
                        <label for="" class="form-label">Precio ($)</label>
                        <input pattern="[0-9]+" id="precio" name="precio" type="number" step="any" class="form-control" value="">
                        </div>
                        <div class="mb-3">
                        <label for="" class="form-label">Descripción</label>
                        <input pattern="[A-Za-z0-9 .-]+" id="descripcion" name="descripcion" type="text" step="any" class="form-control" value="">
                        </div>
                       
                        <button type="button" id="btnEdit" class="btn btn-primary">Editar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </form>
                    </div>
                <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- Modal Edit-->

<!-- Modal Show-->
<div class="modal fade" id="modalShow" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Mostrar productos</h3>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
                <div class="modal-body">
                <form data-action="" method="POST">
                    <input name="id" id="id" type="hidden" value="">
                    <div class="mb-3">
                    <label for="" class="form-label">Código</label>
                    <input id="codigoEdit" name="codigo" type="text" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Nombre</label>
                    <input id="nombreEdit" name="nombre" type="text" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Cantidad</label>
                    <input id="cantidadEdit" name="cantidad" type="number" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Medida</label>
                    <input id="medidaEdit" name="medidaEdit" type="text" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Precio</label>
                    <input id="precioEdit" name="precio" type="number" step="any" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <div class="mb-3">
                    <label for="" class="form-label">Categoria</label>
                    <input id="categoriaEdit" name="categoriaEdit" type="text" class="form-control" value="" readonly>
                    </div>
                    <label for="" class="form-label">Descripción</label>
                    <input id="descripcionEdit" name="descripcion" type="text" step="any" class="form-control" value="" readonly>
                    </div>
                    <button type="button" data-dismiss="modal" class="btn btn-primary">Cerrar</button>
                    </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- Modal Show-->


<!-- Modal Create-->

<div class="modal fade" id="modalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Agregar producto</h3>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form data-action="" method="POST">
                <div class="mb-3">
                <label for="" class="form-label">Código</label>
                <input pattern="[A-Za-z0-9 ]+" id="codigoCreate" name="codigoCreate" type="text" class="form-control" tabindex="1" required>
                </div>
                <div class="mb-3">
                <label for="" class="form-label">Nombre</label>
                <input pattern="[A-Za-z ]+" id="nombreCreate" name="nombreCreate" type="text" class="form-control" tabindex="2" required>
                </div>
                <div class="mb-3">
                <label for="" class="form-label">Cantidad</label>
                <input pattern="[0-9]+" id="cantidadCreate" name="cantidadCreate" type="number" class="form-control" tabindex="3" required>
                </div>

                <label for="" class="form-label">Medida</label>
                <select id="medidaCreate" name="medidaCreate" class="form-control">
                <option value="">-- Escoja la medida del producto --</option>
                <option id="" name="" value="1">L.</option>
                <option id="" name="" value="2">Kg.</option>
                <option id="" name="" value="3">Unidades</option>
                </select>


                <div class="mb-3">
                <label for="" class="form-label">Precio($)</label>
                <input pattern="[0-9]+" id="precioCreate" name="precioCreate" type="number" step="any" class="form-control" tabindex="4" required>
                </div>
                <div class="mb-3">
                <label for="" class="form-label">Descripción</label>
                <input pattern="[A-Za-z0-9 .-]+" id="descripcionCreate" name="descripcionCreate" type="text" step="any" class="form-control" tabindex="5" required>
                </div>
                <div class="mb-3">
                <label for="" class="form-label">Categoria</label>
                <select id="categoriaCreate" name="categoriaCreate" class="form-control">
                <option disabled selected value="">-- Escoja la categoria del producto --</option>
                    @foreach($categorias as $categoria)
                <option id="categorias" name="categorias" value="{{$categoria['id']}}">{{$categoria['categoria_nombre']}}</option>
                    @endforeach
                </select>
                </div>
                <div>
                <button type="button" id="btnCreate" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Modal Create-->