<!-- Modal Edit-->
<div class="modal fade" id="modalEdit" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Editar categoria</h3>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
                <div class="modal-body">
                <form data-action="" method="POST">
                    <input name="idEdit" id="idEdit" type="hidden" value="">
                    <div class="mb-3">
                    <label for="" class="form-label">Nombre de categoria</label>
                    <input pattern="[A-Za-z0-9 ]+" id="nombreEdit" name="nombreEdit" type="text" class="form-control" value="">
                    
                  </div>
                    <button type="button" id="btnEdit" class="btn btn-primary">Editar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!-- Modal Edit-->

<!-- Modal Create-->

<div class="modal fade" id="modalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Agregar categoria</h3>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form data-action="" method="POST">
                    <div class="mb-3">
                    <label for="" class="form-label">Nombre de categoria</label>
                    <input id="nombreCreate" name="nombreCreate" type="text" class="form-control" value=""> 
                    @error('categoria_nombre')
                    <div class="alert-danger">{{$errors->first('categoria_nombre')}}</div>
                    @enderror
                  </div>
                <div class="">
                <button type="button" id="btnCreate" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>    
                </div>
            </form>
      </div>
    </div>
  </div>
</div>