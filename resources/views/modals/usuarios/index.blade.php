<!-- Modal Edit-->
<div class="modal fade" id="modalEdit" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Editar usuario</h3>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
                <div class="modal-body">
                <form data-action="" method="POST">
                        <input name="id" id="id" type="hidden" value="">
                        <div class="mb-3">
                        <label for="" class="form-label">Usuario</label>
                        <input id="usuario" name="usuario" type="text" class="form-control" value="">
                        </div>
                        <div class="mb-3">
                        <label for="" class="form-label">Email</label>
                        <input id="email" name="email" type="email" class="form-control" value="">
                        </div>
                        <div class="mb-3">
                        <label for="" class="form-label">Contraseña</label>
                        <input id="password" name="password" type="text" class="form-control" value="">
                        </div>
                        <div class="mb-3">
                        <label for="" class="form-label">Rol de usuario</label>
                        <select id="rol" name="rol" class="form-control">
                        <option id="valorRol" name="valorRol" value="">--Escoja el rol de usuario--</option>
                        <option value="1">Administrador</option>
                        <option value="2">Cajero</option>
                        </select>
                        </div>
                        <button type="button" id="btnEdit" class="btn btn-primary">Editar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </form>
                    </div>
                <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- Modal Edit-->

<!-- Modal Show-->
<div class="modal fade" id="modalShow" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Detalles de usuario</h3>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
                <div class="modal-body">
                <form data-action="" method="POST">
                    <input name="id" id="id" type="hidden" value="">
                    <div class="mb-3">
                    <label for="" class="form-label">Usuario</label>
                    <input id="usuarioEdit" name="usuarioEdit" type="text" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Email</label>
                    <input id="emailEdit" name="emailEdit" type="email" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Rol de usuario</label>
                    <input id="rolEdit" name="rolEdit" type="text" class="form-control" value="" readonly>
                    </div>
                    <button type="button" data-dismiss="modal" class="btn btn-primary">Cerrar</button>
                    </form>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- Modal Show-->


<!-- Modal Create-->

<div class="modal fade" id="modalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Agregar usuario</h3>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form data-action="" method="POST">
                <div class="mb-3">
                <label for="" class="form-label">Usuario</label>
                <input id="usuarioCreate" name="usuarioCreate" type="text" class="form-control" tabindex="1">
                </div>
                <div class="mb-3">
                <label for="" class="form-label">Email</label>
                <input id="emailCreate" name="emailCreate" type="email" class="form-control" tabindex="2">
                </div>
                <div class="mb-3">
                <label for="" class="form-label">Contraseña</label>
                <input id="passwordCreate" name="passwordCreate" type="text" class="form-control" tabindex="3">
                </div>

                <div class="mb-3">
                <label for="" class="form-label">Rol de usuario</label>
                <select id="rolCreate" name="rolCreate" class="form-control">
                <option value="">-- Escoja el rol del usuario --</option>
                <option name="rol" value="1">Administrador</option>
                <option name="rol" value="2">Cajero</option>
                </select>
                </div>

            <div class="mb-3">
             <label for="" class="form-label">Permisos de usuario</label>
                <select id="permisoCreate" name="permisoCreate" class="form-control">
                    <option disabled selected value="">-- Escoja el permiso del usuario --</option>
                        @foreach($roles as $rol)
                            <option id="permiso" name="permiso" value="{{$rol['id']}}">{{$rol['name']}}</option>
                        @endforeach
                    </select>
            </div>


                <div>
                <button type="button" id="btnCreate" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
                </form>
            </div>
        <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Modal Create-->