<!-- Modal Edit-->
<div class="modal fade" id="modalEdit" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Editar proveedor</h3>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
                <div class="modal-body">
                <form data-action="" method="POST">
                    <input name="idEdit" id="idEdit" type="hidden" value="">
                    <div class="mb-3">
                    <label for="" class="form-label">Nombre de proveedor</label>
                    <input id="nombreProvEdit" name="nombreProvEdit" type="text" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Tipo de documento</label>
                    <select id="tipoEdit" name="tipoEdit" class="form-control">
                    <option value="V">V</option>
                    <option value="J">J</option>
                    <option value="E">E</option>
                    </select>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Número de documento</label>
                    <input id="dniEdit" name="dniEdit" type="text" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Email</label>
                    <input id="emailEdit" name="emailEdit" type="email" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Telefono</label>
                    <input id="telefonoEdit" name="telefonoEdit" type="number" step="any" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Nombre del vendedor</label>
                    <input id="vendedorEdit" name="vendedorEdit" type="text" step="any" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Dirección</label>
                    <input id="direccionEdit" name="direccionEdit" type="text" step="any" class="form-control" value="">
                    </div>
                    <button type="button" id="btnEdit" class="btn btn-primary">Editar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<!-- Modal Edit-->

<!-- Modal Show-->
<div class="modal fade" id="modalShow" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Detalles de proveedor</h3>
                <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
            </div>
                <div class="modal-body">
                <form data-action="" method="POST">
                    <input name="idShow" id="idShow" type="hidden" value="">
                    <div class="mb-3">
                    <label for="" class="form-label">Nombre de proveedor</label>
                    <input id="nombreProvShow" name="nombreProvShow" type="text" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Tipo de documento</label>
                    <input id="tipoShow" name="tipoShow" type="text" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Numero de documento</label>
                    <input id="dniShow" name="dniShow" type="text" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Email</label>
                    <input id="emailShow" name="emailShow" type="email" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Telefono</label>
                    <input id="tlfShow" name="tlfShow" type="text" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Nombre del vendedor</label>
                    <input id="vendedorShow" name="vendedorShow" type="text" class="form-control" value="" readonly>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Dirección</label>
                    <input id="direccionShow" name="direccionShow" type="text" class="form-control" value="" readonly>
                    </div>
                    <button type="button" data-dismiss="modal" class="btn btn-primary">Cerrar</button>
                    </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Show-->

<!-- Modal Create-->

<div class="modal fade" id="modalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Agregar proveedor</h3>
        <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form data-action="" method="POST">
                    <div class="mb-3">
                    <label for="" class="form-label">Nombre de proveedor</label>
                    <input id="nombreProvCreate" name="nombreProvCreate" type="text" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Tipo de documento</label>
                    <select id="tipoCreate" name="tipoCreate" class="form-control">
                    <option value="V">V</option>
                    <option value="J">J</option>
                    <option value="E">E</option>
                    </select>
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Número de documento</label>
                    <input id="dniCreate" name="dniCreate" type="number" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Email</label>
                    <input id="emailCreate" name="emailCreate" type="email" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Telefono</label>
                    <input id="telefonoCreate" name="telefonoCreate" type="number" step="any" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Nombre del vendedor</label>
                    <input id="vendedorCreate" name="vendedorCreate" type="text" step="any" class="form-control" value="">
                    </div>
                    <div class="mb-3">
                    <label for="" class="form-label">Dirección</label>
                    <input id="direccionCreate" name="direccionCreate" type="text" step="any" class="form-control" value="">
                    </div>
                <div class="">
                <button type="button" id="btnCreate" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>    
                </div>
            </form>
      </div>
    </div>
  </div>
</div>