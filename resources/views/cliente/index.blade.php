@extends('main')

@section('title', 'Clientes')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Clientes') }}
        </h2>
        <input type="hidden" value="{{ csrf_token() }}" id="token">


<a href="{{route('PDFClientes')}}" class="btn btn-danger float-right"><i class="fa-solid fa-file-pdf"></i> Generar PDF</a>
<a data-toggle="modal" data-target="#modalCreate" class="btn btn-primary modCreate"><i class="fa-solid fa-plus"></i> Nuevo</a>

<br><br>
<div class ="card">
    <div class ="card-body">
    <table id="clientes" class="table table-hover table-striped table-bordered table-sm mt-4" style="width:100%">
  <thead>
      <tr >
          <th scope="col">Nombre</th>
          <th scope="col">Apellido</th>
          <th scope="col">Documento</th>
          <th scope="col">Teléfono</th>
          <th scope="col">Correo</th>
          <th style="width:250px" scope="col">Acciones</th>
      </tr>
  </thead>
  <tbody>
  </tbody>
</table>
    </div>
</div>

@include('modals.cliente.index')
@endsection
@section('js')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function() {

        clientes = $('#clientes').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por paginas",
            "zeroRecords": "No se encontraron registros",
            "info": "Mostrando  _END_ registros de un total de _TOTAL_ registros",
             
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "No se encontraron registros",
            "search": "Buscar",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",

                "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },

        'ajax':{
            'url':'clienteData',
            'dataSrc':''
        },
        'columns':[
            {'data':'nombre_cliente'},
            {'data':'apellido_cliente'},
            {'data':'dni'},
            {'data':'telefono'},
            {'data':'email'},
            {'data': 'botones'},
        ]
    });


        //---JS para obtener datos---//
        $(document).on('click', '.process', function () {

            var dni= $(this).data('dni');
            var token = $("#token").val();
                $.ajax({
                url:'clientefind',
                headers:{'X-CSRF-TOKEN': token},
                method:'post',
                dataType:'json',
                data: {
                    dni: dni,
                }
            }).done(function(cliente) {
                $("#nombreShow").val(cliente.nombre_cliente);
                $("#apellidoShow").val(cliente.apellido_cliente);
                $("#tipoShow").val(cliente.tipo_documento);
                $("#dniShow").val(cliente.dni);
                $("#emailShow").val(cliente.email);
                $("#tlfShow").val(cliente.telefono);
                $("#direccionShow").val(cliente.direccion);

                $("#id").val(cliente.id);
                $("#nombreEdit").val(cliente.nombre_cliente);
                $("#apellidoEdit").val(cliente.apellido_cliente);
                $("#tipoEdit").val(cliente.tipo_documento);
                $("#dniEdit").val(cliente.dni);
                $("#emailEdit").val(cliente.email);
                $("#telefonoEdit").val(cliente.telefono);
                $("#direccionEdit").val(cliente.direccion);
                
            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'No se pudo obtener los datos del cliente',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        });

        
        //---JS para eliminar---//

        $(document).on('click', '.delete', function () {

            Swal.fire({
                title: '¿Deseas eliminar este cliente?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                    var dni= $(this).data('dni');
                    var token = $("#token").val();
                    $.ajax({
                    url:'clienteDestroy',
                    headers:{'X-CSRF-TOKEN': token},
                    method:'post',
                    dataType:'json',
                    data: {
                    dni: dni,
                    }
            }).done(function(cliente) {
                
                 Swal.fire(
                    cliente.tittle,
                    cliente.msg,
                    cliente.code
                )

                setTimeout( function () {
                    clientes.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El cliente no ha podido ser Eliminado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
        
        //---JS para el EDITAR---//
        $(document).on('click','#btnEdit', function () {
            var id= $("#id").val();
            var nombre_cliente= $("#nombreEdit").val();
            var apellido_cliente= $("#apellidoEdit").val();
            var tipo_documento= $("#tipoEdit").val();
            var dni= $("#dniEdit").val();
            var email= $("#emailEdit").val();
            var telefono= $("#telefonoEdit").val();
            var direccion= $("#direccionEdit").val();

            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas actualizar este cliente?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Actualizar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'clienteupdate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    id:id,
                    nombre_cliente:nombre_cliente,
                    apellido_cliente:apellido_cliente,
                    tipo_documento:tipo_documento,
                    dni:dni,
                    email:email,
                    telefono:telefono,
                    direccion:direccion,
                }
            }).done(function(cliente) {
                
                Swal.fire(
                    cliente.tittle,
                    cliente.msg,
                    cliente.code
                )

                setTimeout( function () {
                    $('#modalEdit').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    clientes.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El cliente no ha podido ser actualizado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });

        //---JS PARA CREAR---//
        $(document).on('click', '#btnCreate', function () {
            var nombre_cliente= $("#nombreCreate").val();
            var apellido_cliente= $("#apellidoCreate").val();
            var tipo_documento= $("#tipoCreate").val();
            var dni= $("#dniCreate").val();
            var email= $("#emailCreate").val();
            var telefono= $("#telefonoCreate").val();
            var direccion= $("#direccionCreate").val();
            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas registrar este cliente?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'agregar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'clienteCreate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    nombre_cliente:nombre_cliente,
                    apellido_cliente:apellido_cliente,
                    tipo_documento:tipo_documento,
                    dni:dni,
                    email:email,
                    telefono:telefono,
                    direccion:direccion,
                }
            }).done(function(cliente) {
                
                Swal.fire(
                    cliente.tittle,
                    cliente.msg,
                    cliente.code
                )

                setTimeout( function () {
                    $('#modalCreate').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    clientes.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El cliente no pudo ser registrado!',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
});


</script>

@endsection