@extends('main')

@section('title', 'Productos')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Productos') }}
        </h2>
        <input type="hidden" value="{{ csrf_token() }}" id="token">


<a href="{{route('PDFProductos')}}" class="btn btn-danger float-right"><i class="fa-solid fa-file-pdf"></i> Generar PDF</a>
<a data-toggle="modal" data-target="#modalCreate" class="btn btn-primary modCreate"><i class="fa-solid fa-plus"></i> Nuevo</a>
<a href="categorias" class="btn btn-primary"><i class="fa-solid fa-plus"></i>Administrar categorias</a>

<br><br>
<div class ="card">
    <div class ="card-body">
    <table id="articulos" class="table table-hover table-striped table-bordered table-sm mt-4" style="width:100%">
  <thead>
      <tr >
          <th scope="col">ID</th>
          <th scope="col">Código</th>
          <th scope="col">Nombre</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Medida</th>
          <th scope="col">Precio ($)</th>
          <th scope="col">Categoria</th>
          <th style="width:250px" scope="col">Acciones</th>
      </tr>
  </thead>
  <tbody>
  </tbody>
</table>
    </div>
</div>

@include('modals.producto.index')
@endsection
@section('js')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function() {

    articulos = $('#articulos').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por paginas",
            "zeroRecords": "No se encontraron registros",
            "info": "Mostrando  _END_ registros de un total de _TOTAL_ registros",
             
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "No se encontraron registros",
            "search": "Buscar",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",

                "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },

        'ajax':{
            'url':'productoData',
            'dataSrc':''
        },
        'columns':[
            {'data':'id'},
            {'data':'codigo'},
            {'data':'nombre'},
            {'data':'cantidad'},
            {'data':'nombre_medida'},
            {'data':'precio'},
            {'data':'categoria_nombre'},
            {'data': 'botones'},
        ]
    });


        //---JS para obtener datos---//
        $(document).on('click', '.process', function () {

            var id= $(this).data('id');
            var token = $("#token").val();
                $.ajax({
                url:'productosfind',
                headers:{'X-CSRF-TOKEN': token},
                method:'post',
                dataType:'json',
                data: {
                    id: id,
                }
            }).done(function(producto) {
                $("#id").val(producto.id);
                $("#codigo").val(producto.codigo);
                $("#nombre").val(producto.nombre);
                $("#cantidad").val(producto.cantidad);
                $("#precio").val(producto.precio);
                $("#descripcion").val(producto.descripcion);

                $("#idEdit").val(producto.id);
                $("#codigoEdit").val(producto.codigo);
                $("#nombreEdit").val(producto.nombre);
                $("#cantidadEdit").val(producto.cantidad);
                $("#medidaEdit").val(producto.nombre_medida);
                $("#precioEdit").val(producto.precio);
                $("#categoriaEdit").val(producto.categoria_nombre);
                $("#descripcionEdit").val(producto.descripcion);
                
            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'No se pudo obtener los datos del producto',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        });

        
        //---JS para eliminar---//

        $(document).on('click', '.delete', function () {

            Swal.fire({
                title: '¿Deseas eliminar este producto?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                    var id= $(this).data('id');
                    var token = $("#token").val();
                    $.ajax({
                    url:'productosDestroy',
                    headers:{'X-CSRF-TOKEN': token},
                    method:'post',
                    dataType:'json',
                    data: {
                    id: id,
                    }
            }).done(function(producto) {
                
                 Swal.fire(
                 producto.tittle,
                 producto.msg,
                 producto.code
                )

                setTimeout( function () {
                    articulos.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El producto no ha podido ser Eliminado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
        
        //---JS para el EDITAR---//
        $(document).on('click','#btnEdit', function () {
            var id= $("#id").val();
            var codigo= $("#codigo").val();
            var nombre= $("#nombre").val();
            var cantidad= $("#cantidad").val();
            var precio= $("#precio").val();
            var descripcion= $("#descripcion").val();

            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas actualizar este producto?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Actualizar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'productosupdate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    id:id,
                    codigo:codigo,
                    nombre:nombre,
                    cantidad:cantidad,
                    precio:precio,
                    descripcion:descripcion,
                }
            }).done(function(producto) {
                
                Swal.fire(
                 producto.tittle,
                 producto.msg,
                 producto.code
                )

                setTimeout( function () {
                    $('#modalEdit').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    articulos.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El producto no ha podido ser actualizado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });

        //---JS PARA CREAR---//
        $(document).on('click', '#btnCreate', function () {
            var codigo= $("#codigoCreate").val();
            var nombre= $("#nombreCreate").val();
            var cantidad= $("#cantidadCreate").val();
            var precio= $("#precioCreate").val();
            var descripcion= $("#descripcionCreate").val();
            var categoria= $("#categoriaCreate").val();
            var medida= $("#medidaCreate").val();
            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas Registrar este producto?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'agregar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'productosCreate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    codigo:codigo,
                    nombre:nombre,
                    cantidad:cantidad,
                    precio:precio,
                    descripcion:descripcion,
                    id_tipo_producto:categoria,
                    id_medida:medida,
                }
            }).done(function(producto) {
                
                Swal.fire(
                 producto.tittle,
                 producto.msg,
                 producto.code
                )

                setTimeout( function () {
                    $('#modalCreate').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    articulos.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El producto no pudo ser registrado!',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
});


</script>

@endsection