@extends('main')

@section('title', 'Proveedor')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Proveedores') }}
        </h2>
        <input type="hidden" value="{{ csrf_token() }}" id="token">


        <a href="{{route('PDFProveedores')}}" class="btn btn-danger float-right"><i class="fa-solid fa-file-pdf"></i> Generar PDF</a>
<a data-toggle="modal" data-target="#modalCreate" class="btn btn-primary modCreate"><i class="fa-solid fa-plus"></i> Nuevo</a>

<br><br>
<div class ="card">
    <div class ="card-body">
    <table id="proveedores" class="table table-hover table-striped table-bordered table-sm mt-4" style="width:100%">
  <thead>
      <tr >
          <th scope="col">ID</th>
          <th scope="col">Nombre de proveedor</th>
          <th scope="col">Correo</th>
          <th scope="col">Telefono</th>
          <th scope="col">Dirección</th>
          <th style="width:275px" scope="col">Acciones</th>
      </tr>
  </thead>
  <tbody>
  </tbody>
</table>
    </div>
</div>

@include('modals.proveedor.index')
@endsection
@section('js')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function() {

        proveedores = $('#proveedores').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por paginas",
            "zeroRecords": "No se encontraron registros",
            "info": "Mostrando  _END_ registros de un total de _TOTAL_ registros",
             
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "No se encontraron registros",
            "search": "Buscar",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",

                "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },

        'ajax':{
            'url':'proveedorData',
            'dataSrc':''
        },
        'columns':[
            {'data':'id'},
            {'data':'nombre_proveedor'},
            {'data':'email'},
            {'data':'telefono'},
            {'data':'direccion_fiscal'},
            {'data': 'botones'},
        ]
    });


        //---JS para obtener datos---//
        $(document).on('click', '.process', function () {

            var id= $(this).data('id');
            var token = $("#token").val();
                $.ajax({
                url:'proveedorfind',
                headers:{'X-CSRF-TOKEN': token},
                method:'post',
                dataType:'json',
                data: {
                    id: id,
                }
            }).done(function(proveedor) {
                $("#idShow").val(proveedor.id);
                $("#nombreProvShow").val(proveedor.nombre_proveedor);
                $("#tipoShow").val(proveedor.tipo_documento);
                $("#dniShow").val(proveedor.dni);
                $("#emailShow").val(proveedor.email);
                $("#tlfShow").val(proveedor.telefono);
                $("#vendedorShow").val(proveedor.nombre_vendedor);
                $("#direccionShow").val(proveedor.direccion_fiscal);

                $("#idEdit").val(proveedor.id);
                $("#nombreProvEdit").val(proveedor.nombre_proveedor);
                $("#tipoEdit").val(proveedor.tipo_documento);
                $("#dniEdit").val(proveedor.dni);
                $("#emailEdit").val(proveedor.email);
                $("#telefonoEdit").val(proveedor.telefono);
                $("#vendedorEdit").val(proveedor.nombre_vendedor);
                $("#direccionEdit").val(proveedor.direccion_fiscal);
                
            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'No se pudo obtener los datos del proveedor',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        });

        
        //---JS para eliminar---//

        $(document).on('click', '.delete', function () {

            Swal.fire({
                title: '¿Deseas eliminar este proveedor?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                    var id= $(this).data('id');
                    var token = $("#token").val();
                    $.ajax({
                    url:'proveedorDestroy',
                    headers:{'X-CSRF-TOKEN': token},
                    method:'post',
                    dataType:'json',
                    data: {
                    id: id,
                    }
            }).done(function(proveedor) {
                
                 Swal.fire(
                 proveedor.tittle,
                 proveedor.msg,
                 proveedor.code
                )

                setTimeout( function () {
                    proveedores.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El proveedor no ha podido ser Eliminado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
        
        //---JS para el EDITAR---//
        $(document).on('click','#btnEdit', function () {
            var id= $("#idEdit").val();
            var nombre_proveedor= $("#nombreProvEdit").val();
            var tipo_documento= $("#tipoEdit").val();
            var dni= $("#dniEdit").val();
            var email= $("#emailEdit").val();
            var telefono= $("#telefonoEdit").val();
            var nombre_vendedor= $("#vendedorEdit").val();
            var direccion_fiscal= $("#direccionEdit").val();

            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas actualizar este proveedor?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Actualizar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'proveedorupdate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    id:id,
                    nombre_proveedor:nombre_proveedor,
                    tipo_documento:tipo_documento,
                    dni:dni,
                    email:email,
                    telefono:telefono,
                    nombre_vendedor:nombre_vendedor,
                    direccion_fiscal:direccion_fiscal,
                }
            }).done(function(proveedor) {
                
                Swal.fire(
                proveedor.tittle,
                proveedor.msg,
                proveedor.code
                )

                setTimeout( function () {
                    $('#modalEdit').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    proveedores.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El proveedor no ha podido ser actualizado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });

        //---JS PARA CREAR---//
        $(document).on('click', '#btnCreate', function () {
            var nombre_proveedor= $("#nombreProvCreate").val();
            var tipo_documento= $("#tipoCreate").val();
            var dni= $("#dniCreate").val();
            var email= $("#emailCreate").val();
            var telefono= $("#telefonoCreate").val();
            var nombre_vendedor= $("#vendedorCreate").val();
            var direccion_fiscal= $("#direccionCreate").val();
            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas registrar este proveedor?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'agregar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'proveedorCreate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    nombre_proveedor:nombre_proveedor,
                    tipo_documento:tipo_documento,
                    dni:dni,
                    email:email,
                    telefono:telefono,
                    nombre_vendedor:nombre_vendedor,
                    direccion_fiscal:direccion_fiscal,
                }
            }).done(function(proveedor) {
                
                Swal.fire(
                proveedor.tittle,
                proveedor.msg,
                proveedor.code
                )

                setTimeout( function () {
                    $('#modalCreate').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    proveedores.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El proveedor no pudo ser registrado!',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
});


</script>

@endsection