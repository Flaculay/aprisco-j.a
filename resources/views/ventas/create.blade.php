@extends('main')

@section('title', 'Registro de ventas')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

<h2 class="font-semibold text-xl text-gray-800 leading-tight">
    {{ __('Registrar nueva ventas') }}
</h2>
<input type="hidden" value="{{ csrf_token() }}" id="token">

<br><br>

<div class="card">
    <div class="card-body">
        <form action="{{route ('ventas.store')}}" method="POST">
            @csrf

            <div class="mb-3">
                <label for="provider_id">Cliente</label>
                <select class="form-control" name="cliente" id="cliente">
                    <option value="" disabled selected>Selecccione un cliente</option>
                    @foreach ($clientes as $cliente)
                    <option value="{{$cliente->id}}">{{$cliente->nombre_cliente. ' ' .$cliente->apellido_cliente. ' - ' .$cliente->dni}}
                    </option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label for="provider_id">Trabajador</label>
                <select class="form-control" name="trabajador" id="trabajador">
                    <option value="" disabled selected>Selecccione un trabajador</option>
                    @foreach ($trabajadores as $trabajador)
                    <option value="{{$trabajador->id}}">{{$trabajador->nombre_trabajador. ' ' .$trabajador->apellido_trabajador}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label for="product_id">Producto</label>
                <select class="form-control" name="id_producto1" id="id_producto1" data-live-search="true">
                    <option value="" disabled selected>Selecccione un producto</option>
                    @foreach ($productos as $producto)
                    <option value="{{$producto->id}}">{{$producto->nombre}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label for="" class="form-label">Fecha</label>
                <input id="fecha" name="fecha" type="date" step="any" class="form-control">
            </div>

            <div class="mb-3">
                <div class="form-group">
                    <label for="">Stock actual</label>
                    <input type="number" name="stock" id="stock" value="" class="form-control" disabled>
                </div>
            </div>

            <div class="mb-3">
                <label for="quantity">Cantidad</label>
                <input type="number" class="form-control" name="cantidad" id="cantidad" aria-describedby="helpId">
            </div>
            <div class="mb-3">
                <label for="price">Precio de venta</label>
                <input type="number" class="form-control" name="precio" id="precio" aria-describedby="helpId" disabled>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Metodo de pago</label>
                <select id="metodo" name="metodo" class="form-control">
                    <option value="" disabled selected>Método de pago</option>
                    <option value="Bs. Efectivo">Bs. Efectivo</option>
                    <option value="Bs. Transferencia">Bs. Transferencia</option>
                    <option value="Bs. Pago movil">Bs. Pago movil</option>
                    <option value="Dolares efectivo">Dolares efectivo</option>
                    <option value="Dolares  transferencia">Dolares transferencia</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Banco de pago</label>
                <select id="banco" name="banco" class="form-control">
                    <option value="" disabled selected>Banco al cual se realizó el pago</option>
                    <option value="Banco Provincial">Banco Provincial</option>
                    <option value="Banco Venezuela">Banco Venezuela</option>
                    <option value="Banco Mercantil">Banco Mercantil</option>
                    <option value="Banco Exterior">Banco Exterior</option>
                    <option value="Zelle">Zelle</option>
                    <option value="Reserve">Reserve</option>
                </select>
            </div>
            <div class="mb-3">
                <button type="button" id="agregar" class="btn btn-primary float-right">Agregar producto</button>
            </div>
            <div class="mb-3">
                <h4 class="card-title">Detalles de compra</h4>
                <div class="table-responsive col-md-12">
                    <table id="detalles" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Eliminar</th>
                                <th>Producto</th>
                                <th>Precio(USD)</th>
                                <th>Cantidad</th>
                                <th>SubTotal(USD)</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="4">
                                    <p align="right">TOTAL:</p>
                                </th>
                                <th>
                                    <p align="right"><span id="total">USD 0.00</span> </p>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="4">
                                    <p align="right">TOTAL PAGAR:</p>
                                </th>
                                <th>
                                    <p align="right"><span align="right" id="total_pagar_html">USD 0.00</span> <input
                                            type="hidden" name="total" id="total_pagar"></p>
                                </th>
                            </tr>
                        </tfoot>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="card-footer text-muted">
                        <button type="submit" id="guardar" class="btn btn-primary">Registrar</button>
                        <a href="{{route('ventas.index')}}" class="btn btn-light">Cancelar</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


@endsection
@section('js')

<script>
    $(document).ready(function () {
        $("#agregar").click(function () {
            agregar();
        });


        var product_id1 = $('#id_producto1');


        product_id1.on("change", function () {
            $.ajax({
                url: "{{route('obtener_productos')}}",
                method: 'GET',
                data: {
                    id_producto: product_id1.val(),
                },
                success: function (data) {
                    $("#precio").val(data.precio);
                    $("#stock").val(data.cantidad);
                }
            });
        });
    });

    var cont = 1;
    total = 0;
    subtotal = [];
    $("#guardar").hide();

    function agregar() {
        id_producto = $("#id_producto1").val();
        producto = $("#id_producto1 option:selected").text();
        cantidad = $("#cantidad").val();
        precio = $("#precio").val();
        stock = $("#stock").val();
        if (id_producto != "" && cantidad != "" && cantidad > 0 && precio != "") {
            if (parseInt(stock) >= parseInt(cantidad)) {
                subtotal[cont] = (cantidad * precio);
                total = total + subtotal[cont];
                var fila = '<tr class="selected" id="fila' + cont +
                    '"><td><button type="button" class="btn btn-danger btn-sm" onclick="eliminar(' + cont +
                    ');"><i class="fa fa-times fa-2x"></i></button></td> <td><input type="hidden" name="id_producto[]" value="' +
                    id_producto + '">' + producto + '</td> <td> <input type="hidden" name="precio[]" value="' +
                    parseFloat(precio).toFixed(2) + '"> <input class="form-control" type="number" value="' + parseFloat(
                        precio).toFixed(2) + '" disabled> </td><td> <input type="hidden" name="cantidad[]" value="' +
                    cantidad + '"> <input type="number" value="' + cantidad +
                    '" class="form-control" disabled> </td> <td align="right">USD' + parseFloat(subtotal[cont]).toFixed(
                        2) + '</td></tr>';
                cont++;
                limpiar();
                totales();
                evaluar();
                $('#detalles').append(fila);
            } else {
                Swal.fire({
                    type: 'error',
                    text: 'La cantidad a vender supera el stock.',
                })
            }
        } else {
            Swal.fire({
                type: 'error',
                text: 'Rellene todos los campos del detalle de la venta.',
            })
        }
    }

    function limpiar() {
        $("#cantidad").val("");
    }

    function totales() {
        $("#total").html("USD " + total.toFixed(2));
        total_pagar = total;
        $("#total_pagar_html").html("USD " + total_pagar.toFixed(2));
        $("#total_pagar").val(total_pagar.toFixed(2));
    }

    function evaluar() {
        if (total > 0) {
            $("#guardar").show();
        } else {
            $("#guardar").hide();
        }
    }

    function eliminar(index) {
        total = total - subtotal[index];
        total_pagar_html = total;
        $("#total").html("USD" + total);
        $("#total_pagar_html").html("USD" + total_pagar_html);
        $("#total_pagar").val(total_pagar_html.toFixed(2));
        $("#fila" + index).remove();
        evaluar();
    }

</script>

@endsection
