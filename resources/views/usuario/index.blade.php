@extends('main')

@section('title', 'Usuarios')


@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Usuarios') }}
        </h2>
        <input type="hidden" value="{{ csrf_token() }}" id="token">

<a data-toggle="modal" data-target="#modalCreate" class="btn btn-primary"><i class="fa-solid fa-plus"></i> Nuevo usuario</a>

<br><br>
<div class ="card">
    <div class ="card-body">
    <table id="usuarios" class="table table-hover table-striped table-bordered table-sm mt-4" style="width:100%">
  <thead>
      <tr >
          <th scope="col">ID</th>
          <th scope="col">Usuario</th>
          <th scope="col">Email</th>
          <th scope="col">Tipo de usuario</th>
          <th style="width:250px" scope="col">Acciones</th>
      </tr>
  </thead>
  <tbody>
  </tbody>
</table>
    </div>
</div>


@include('modals.usuarios.index')
@endsection
@section('js')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function() {

        usuarios = $('#usuarios').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por paginas",
            "zeroRecords": "No se encontraron registros",
            "info": "Mostrando  _END_ registros de un total de _TOTAL_ registros",
             
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "No se encontraron registros",
            "search": "Buscar",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",

                "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },

        'ajax':{
            'url':'usuariosData',
            'dataSrc':''
        },
        'columns':[
            {'data':'id'},
            {'data':'name'},
            {'data':'email'},
            {'data':'tipo_usuario'},
            {'data': 'botones'},
        ]
    });


        //---JS para obtener datos---//
        $(document).on('click', '.process', function () {

            var id= $(this).data('id');
            var token = $("#token").val();
                $.ajax({
                url:'usuariosfind',
                headers:{'X-CSRF-TOKEN': token},
                method:'post',
                dataType:'json',
                data: {
                    id: id,
                }
            }).done(function(usuarios) {
                $("#id").val(usuarios.id);
                $("#usuario").val(usuarios.name);
                $("#email").val(usuarios.email);
                $("#password").val(usuarios.password);
                $("#rol").val(usuarios.id_tipo_usuario);


                $("#idEdit").val(usuarios.id);
                $("#usuarioEdit").val(usuarios.name);
                $("#emailEdit").val(usuarios.email);
                $("#rolEdit").val(usuarios.tipo_usuario);
                
            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'No se pudo obtener los datos del usuario',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        });

        
        //---JS para eliminar---//

        $(document).on('click', '.delete', function () {

            Swal.fire({
                title: '¿Deseas eliminar este usuario?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                    var id= $(this).data('id');
                    var token = $("#token").val();
                    $.ajax({
                    url:'usuariosDestroy',
                    headers:{'X-CSRF-TOKEN': token},
                    method:'post',
                    dataType:'json',
                    data: {
                    id: id,
                    }
            }).done(function(producto) {
                
                 Swal.fire(
                 producto.tittle,
                 producto.msg,
                 producto.code
                )

                setTimeout( function () {
                    usuarios.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El usuario no ha podido ser Eliminado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
        
        //---JS para el EDITAR---//
        $(document).on('click','#btnEdit', function () {
            var id= $("#id").val();
            var name= $("#usuario").val();
            var email= $("#email").val();
            var password= $("#password").val();
            var id_tipo_usuario= $("#rol").val();

            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas actualizar este usuario?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Actualizar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'usuariosupdate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    id:id,
                    name:name,
                    email:email,
                    password:password,
                    id_tipo_usuario:id_tipo_usuario,
                    
                }
            }).done(function(producto) {
                
                Swal.fire(
                 producto.tittle,
                 producto.msg,
                 producto.code
                )

                setTimeout( function () {
                    $('#modalEdit').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    usuarios.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El usuario no ha podido ser actualizado!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });

        //---JS PARA CREAR---//
        $(document).on('click', '#btnCreate', function () {
            var name= $("#usuarioCreate").val();
            var email= $("#emailCreate").val();
            var password= $("#passwordCreate").val();
            var id_tipo_usuario= $("#rolCreate").val();
            var permiso= $("#permisoCreate").val();
            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas Registrar este usuario?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Agregar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'usuariosCreate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    name:name,
                    email:email,
                    password:password,
                    id_tipo_usuario:id_tipo_usuario,
                    permiso:permiso,
                }
            }).done(function(producto) {
                
                Swal.fire(
                 producto.tittle,
                 producto.msg,
                 producto.code
                )

                setTimeout( function () {
                    $('#modalCreate').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    usuarios.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'El usuario no pudo ser registrado!',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
});


</script>

@endsection