@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
@stop

@section('content')
<div class="container">
    <div class="card">
        <div class ="m-0 row justify-content-center">
                <div class="col-auto">
                <img src="logo.jpg" alt="">
                </div>
                </div>
                </div>
                </div>
@stop

@section('css')
    <script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@stop