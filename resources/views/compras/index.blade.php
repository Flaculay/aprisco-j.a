@extends('main')

@section('title', 'Compras')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Compras') }}
        </h2>
        <input type="hidden" value="{{ csrf_token() }}" id="token">

<a href="compras/create" class="btn btn-primary"><i class="fa-solid fa-plus"></i> Nuevo</a>

<br><br>
<div class ="card">
    <div class ="card-body">
    <table id="compras" class="table table-hover table-striped table-bordered table-sm mt-4" style="width:100%">
  <thead>
      <tr >
          <th style="width:150px" scope="col">Numero de factura</th>
          <th scope="col">Fecha</th>
          <th scope="col">Total</th>
          <th scope="col">Proveedor</th>
          <th style="width:150px" scope="col">Acciones</th>
      </tr>
  </thead>
  <tbody>
      @foreach($compras as $compra)
      <tr>
                <td>{{$compra->id}}</td>
                <td>{{\Carbon\Carbon::parse($compra->fecha_compra)->format('d, M  Y')}}</td>
                <td>{{$compra->total_pagar}}</td>
                <td>{{$compra->nombre_proveedor}}</td>
                <td>
                <form action="{{route ('compras.destroy', $compra->id)}}" method="POST">
                    <a href="{{route('compras.show', $compra->id)}}" class="btn btn-outline-danger btn-sm"><i class="fa-solid fa-file-pdf"></i> PDF</a>
                    @csrf
                    @method('DELETE')
                    @can('compras.destroy')
                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa-solid fa-trash-can"></i>&nbsp Eliminar</button>
                    @endcan
                    
                </form>
                </td>
            </tr>
      @endforeach
  </tbody>
</table>
    </div>
</div>

@endsection
@section('js')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function() {

        compras = $('#compras').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por paginas",
            "zeroRecords": "No se encontraron registros",
            "info": "Mostrando  _END_ registros de un total de _TOTAL_ registros",
             
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "No se encontraron registros",
            "search": "Buscar",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",

                "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },
    });
});

</script>
@endsection