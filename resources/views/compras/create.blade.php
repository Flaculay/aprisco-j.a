@extends('main')

@section('title', 'Registro de compras')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

<h2 class="font-semibold text-xl text-gray-800 leading-tight">
    {{ __('Registrar compra') }}
</h2>
<input type="hidden" value="{{ csrf_token() }}" id="token">

<br><br>

<div class="card">
    <div class="card-body">
        <form action="{{route ('compras.store')}}" method="POST">
            @csrf

            <div class="mb-3">
                <label for="provider_id">Proveedor</label>
                <select class="form-control" name="proveedor" id="proveedor">
                    <option value="" disabled selected>Selecccione un proveedor</option>
                    @foreach ($proveedores as $proveedor)
                    <option value="{{$proveedor->id}}">{{$proveedor->nombre_proveedor}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="product_id">Producto</label>
                <select class="form-control" name="id_producto" id="id_producto">
                    <option value="" disabled selected>Selecccione un producto</option>
                    @foreach ($productos as $producto)
                    <option value="{{$producto->id}}">{{$producto->nombre}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label for="" class="form-label">Fecha</label>
                <input id="fecha" name="fecha" type="date" step="any" class="form-control">
            </div>

            <div class="mb-3">
                <label for="quantity">Cantidad</label>
                <input type="number" class="form-control" name="cantidad" id="cantidad" aria-describedby="helpId">
            </div>
            <div class="mb-3">
                <label for="price">Precio de compra</label>
                <input type="number" class="form-control" name="costo" id="costo" aria-describedby="helpId">
            </div>
            <div class="mb-3">
                <label for="price">Precio de venta</label>
                <input type="number" class="form-control" name="precio" id="precio" aria-describedby="helpId">
            </div>
            <div class="mb-3">
                <button type="button" id="agregar" class="btn btn-primary float-right">Agregar producto</button>
            </div>
            <div class="mb-3">
                <h4 class="card-title">Detalles de compra</h4>
                <div class="table-responsive col-md-12">
                    <table id="detalles" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Eliminar</th>
                                <th>Producto</th>
                                <th>Precio(USD)</th>
                                <th>Cantidad</th>
                                <th>SubTotal(USD)</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="5">
                                    <p align="right">TOTAL:</p>
                                </th>
                                <th>
                                    <p align="right"><span id="total">USD 0.00</span> </p>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="5">
                                    <p align="right">TOTAL PAGAR:</p>
                                </th>
                                <th>
                                    <p align="right"><span align="right" id="total_pagar_html">USD 0.00</span> <input
                                            type="hidden" name="total" id="total_pagar"></p>
                                </th>
                            </tr>
                        </tfoot>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="card-footer text-muted">
                        <button type="submit" id="guardar" class="btn btn-primary">Registrar</button>
                        <a href="{{route('compras.index')}}" class="btn btn-light">Cancelar</a>
                    </div>
        </form>
    </div>
</div>


@endsection
@section('js')

<script>
    $(document).ready(function () {
        $("#agregar").click(function () {
            agregar();
        });
    });

    var cont = 0;
    total = 0;
    subtotal = [];

    function agregar() {
        id_proveedor = $('#proveedor').val();
        fecha_compra = $("#fecha").val();
        id_producto = $("#id_producto").val();
        producto = $("#id_producto option:selected").text();
        cantidad = $("#cantidad").val();
        precio = $("#precio").val();
        costo = $("#costo").val();

        if (id_producto != "" && cantidad != "" && cantidad > 0 && costo != "" && precio != "") {
            subtotal[cont] = cantidad * costo;
            total = total + subtotal[cont];
            var fila = '<tr class="selected" id="fila' + cont +
                '"><td><button type="button" class="btn btn-danger btn-sm" onclick="eliminar(' + cont +
                ');"><i class="fa fa-times"></i></button></td><td><input type="hidden" name="id_producto[]" value="' +
                id_producto + '">' + producto +
                '</td> !!<td> <input type="hidden" id="costo[]" name="costo[]" value="' + costo +
                '"> <input class="form-control" type="number" id="costo[]" value="' + costo +
                '" disabled> </td> <td> <input type="hidden" id="precio[]" name="precio[]" value="' + precio +
                '"> <input class="form-control" type="number" id="precio[]" value="' + precio +
                '" disabled> </td>  <td> <input type="hidden" name="cantidad[]" value="' + cantidad +
                '"> <input class="form-control" type="number" value="' + cantidad +
                '" disabled> </td> <td align="right">USD ' + subtotal[cont] + ' </td></tr>';
            cont++;
            totales();
            evaluar();
            total_pagar = total;
            $('#detalles').append(fila);
            limpiar();
        } else {
            Swal.fire({
                type: 'error',
                text: 'Rellene todos los campos del detalle de la compras',
            })
        }
    }

    function limpiar() {
        $("#costo").val("");
        $("#cantidad").val("");
        $("#precio").val("");
    }

    function totales() {
        $("#total").html("USD " + total.toFixed(2));
        total_pagar = total;
        $("#total_pagar_html").html("USD " + total_pagar.toFixed(2));
        $("#total_pagar").val(total_pagar.toFixed(2));
    }

    function evaluar() {
        if (total > 0) {
            $("#guardar").show();
        } else {
            $("#guardar").hide();
        }
    }

    function eliminar(index) {
        total = total - subtotal[index];
        total_pagar_html = total;
        $("#total").html("USD" + total);
        $("#total_pagar_html").html("USD" + total_pagar_html);
        $("#total_pagar").val(total_pagar_html.toFixed(2));
        $("#fila" + index).remove();
        evaluar();
    }

</script>

@endsection
