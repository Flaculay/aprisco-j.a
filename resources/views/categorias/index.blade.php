@extends('main')

@section('title', 'Categorias')

@section('css')

<script src="https://kit.fontawesome.com/add63969fd.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
@endsection

@section('content')

        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Administrar categorias') }}
        </h2>
        <input type="hidden" value="{{ csrf_token() }}" id="token">

<a data-toggle="modal" data-target="#modalCreate" class="btn btn-primary modCreate"><i class="fa-solid fa-plus"></i> Nuevo</a>

<br><br>
<div style="width:700px" class="card">
    <div class ="card-body">
    <table style="width:650px" id="categorias" class="table table-hover table-striped table-bordered table-sm mt-4" style="width:100%">
  <thead>
      <tr >
          <th scope="col">ID</th>
          <th scope="col">Nombre de categoria</th>
          <th style="width:150px" scope="col">Acciones</th>
      </tr>
  </thead>
  <tbody>
  </tbody>
</table>
    </div>
</div>

@include('modals.categorias.index')
@endsection
@section('js')

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(document).ready(function() {

        categoria = $('#categorias').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por paginas",
            "zeroRecords": "No se encontraron registros",
            "info": "Mostrando  _END_ registros de un total de _TOTAL_ registros",
             
            "emptyTable": "Ningún dato disponible en esta tabla",
            "infoEmpty": "No se encontraron registros",
            "search": "Buscar",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",

                "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },

        'ajax':{
            'url':'categoriasData',
            'dataSrc':''
        },
        'columns':[
            {'data':'id'},
            {'data':'categoria_nombre'},
            {'data': 'botones'},
        ]
    });


        //---JS para obtener datos---//
        $(document).on('click', '.process', function () {

            var id= $(this).data('id');
            var token = $("#token").val();
                $.ajax({
                url:'categoriasfind',
                headers:{'X-CSRF-TOKEN': token},
                method:'post',
                dataType:'json',
                data: {
                    id: id,
                }
            }).done(function(categorias) {

                $("#idEdit").val(categorias.id);
                $("#nombreEdit").val(categorias.categoria_nombre);
                
            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'No se pudo obtener los datos de la categoria',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        });

        
        //---JS para eliminar---//

        $(document).on('click', '.delete', function () {

            Swal.fire({
                title: '¿Deseas eliminar esta categoria?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                    var id= $(this).data('id');
                    var token = $("#token").val();
                    $.ajax({
                    url:'categoriasDestroy',
                    headers:{'X-CSRF-TOKEN': token},
                    method:'post',
                    dataType:'json',
                    data: {
                    id: id,
                    }
            }).done(function(categorias) {
                
                 Swal.fire(
                    categorias.tittle,
                    categorias.msg,
                    categorias.code
                )

                setTimeout( function () {
                    categoria.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'La categoria no ha podido ser eliminada!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
        
        //---JS para el EDITAR---//
        $(document).on('click','#btnEdit', function () {
            var id= $("#idEdit").val();
            var categoria_nombre = $("#nombreEdit").val();

            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas actualizar esta categoria?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Actualizar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'categoriasupdate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    id:id,
                    categoria_nombre:categoria_nombre,
 
                }
            }).done(function(categorias) {
                
                Swal.fire(
                    categorias.tittle,
                    categorias.msg,
                    categorias.code
                )

                setTimeout( function () {
                    $('#modalEdit').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    categoria.ajax.reload( null, false );
                    }, 1000 );

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'La categoria no ha podido ser actualizada!',
                'error',   
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });

        //---JS PARA CREAR---//
        $(document).on('click', '#btnCreate', function () {
            var categoria_nombre = $("#nombreCreate").val();
            var token = $("#token").val();

            Swal.fire({
                title: '¿Deseas registrar esta categoria?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'agregar',
                cancelButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                url:'categoriasCreate',
                headers: {'X-CSRF-TOKEN': token},
                method: 'post',
                dataType: 'json',
                data: {
                    categoria_nombre:categoria_nombre,
                }
            }).done(function(categorias) {
                
                Swal.fire(
                    categorias.tittle,
                    categorias.msg,
                    categorias.code
                )

                setTimeout( function () {
                    $('#modalCreate').modal('hide');
                    }, 1000 );

                setTimeout( function () {
                    categoria.ajax.reload( null, false );
                    },1000);

            }).fail(function (json) {
                Swal.fire(
                'Error!',
                'La categoria no pudo ser registrada!',
                'error'
            )
            }).always(function() {
                $('#process').button('reset');
            });
        }})
    });
});


</script>

@endsection